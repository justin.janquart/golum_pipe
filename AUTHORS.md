This file lists the names of authors having contributed to the development of this package, through code and discussion. If your name is not listed here and you think it should be, please contact us.

Angel Garron, Justin Janquart, David Keitel, Laura Uronen, Daniel Williams, Mick Wright
