"""
Script used to set up the joint PE analysis
"""

import re
import sys
from golum_pipe.data_analysis.joint_pe_data_analysis import main


if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())