"""
Script used to analyze the second image data
(replace the bily_one to parse the likelihood 
arguments properly)
"""

import re
import sys
from golum_pipe.data_analysis.image_2_data_analysis import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())