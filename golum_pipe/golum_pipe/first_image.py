"""
Script taking care of starting the first image run.
Reads the .ini file and starts the rest using 
bilby_pipe
"""

import argparse
import bilby
import bilby_pipe
import shlex
import subprocess

from golum_pipe.core import utils as utls
from golum_pipe.core.utils import golum_logger
from golum_pipe.core import golum_parsers, to_bilby_pipe
from golum_pipe.job_generation import dag_generation


def main():
    """
    Function actually reading the command lie and the 
    configuration ini file. It uses it to create the 
    dag file to run the first image analysis.

    Since it is a simple modification of the waveform 
    model, we just make a wrapper around bilby_pipe. 
    """

    # start by parsing the arguments
    golumparser = golum_parsers.set_golum_parser()
    args = golumparser.parse_args()

    # verify whether the config file exist and can be read
    config = utls.verify_config_file(args)

    # check whether one is running an injection or not
    if config.getboolean('injection_settings', 'injection'):
        args.injection = True
    if config.getboolean('condor_settings', 'local'):
        args.local = True

    # setup the output directories
    outdir = config.get('output_settings', 'outdir', fallback = ".")
    output_directories = {"outdir" : outdir, "data" : f"{outdir}/data",
                          "submit" : f"{outdir}/submit"}
    for _, directory in output_directories.items():
        bilby.core.utils.check_directory_exists_and_if_not_mkdir(directory)

    # setup the logger
    logging_level = config.get('output_settings', 'logging-level', fallback = 'INFO')
    utls.setup_logger(output_directories['outdir'], logging_level, args = args)
    outdir = output_directories['outdir']
    golum_logger.info(f'Golum logger is setup, log output in {outdir}/golum_img1.log')

    # check if the analysis is an injection or not
    if args.injection or config.getboolean('injection_settings', 'injection'):
        injection_waveform_arguments = utls.make_waveform_arguments(config, injection = True)
        golum_logger.info('Injection waveform arguments: %s', injection_waveform_arguments)
        golum_logger.info('Setting the injection up')
        injection_file = utls.make_injection_file(config)
        golum_logger.info('Injection file generated')
    else:
        injection_file = None
        injection_waveform_arguments = None

    # read in the information for the analysis
    golum_logger.info('Generating analysis waveform argument')
    analysis_waveform_arguments = utls.make_waveform_arguments(config)
    golum_logger.info('Analysis waveform arguments: %s', analysis_waveform_arguments)

    # run the bilby pipe functions to set things up
    bilby_pipe_config = to_bilby_pipe.make_bilby_pipe_config(config, args, output_directories,
                                               injection_file = injection_file,
                                               injection_waveform_arguments = injection_waveform_arguments,
                                               analysis_waveform_arguments = analysis_waveform_arguments)


    bilby_pipe_args, bilby_pipe_unknown_args, bilby_pipe_parser = to_bilby_pipe.get_bilby_pipe_arguments(bilby_pipe_config)
    bilby_pipe_input = bilby_pipe.main.MainInput(bilby_pipe_args, bilby_pipe_unknown_args)
    bilby_pipe_input.pretty_print_prior()
    bilby_pipe.main.write_complete_config_file(bilby_pipe_parser, bilby_pipe_args, bilby_pipe_input)
    dag_generation.generate_dag_first_image(bilby_pipe_input)

    golum_logger.info('Bilby pipe file generation DONE')

    # make the final dag if needed
    if not args.local or config.getboolean('run_settings', 'local', fallback = False):
        golum_logger.info('Constructing final dag')
        final_dag = utls.make_final_dag(config, output_directories, img = 1)
              
        if config.getboolean('condor_settings', 'submit', fallback = False):
            golum_logger.info(f"Submitting dag {final_dag} to condor")

            command = f"condor_submit_dag {final_dag}"
            submit_dag_proc = subprocess.Popen(shlex.split(command), stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = submit_dag_proc.communicate()
            golum_logger.info(f'{out}')

        else:
            golum_logger.info('To submit, use the following command: \n\t$ condor_submit_dag %s', final_dag)

if __name__ == "__main__":
    main()
