"""
Script to take care of the dag generation for the second image.
This is an adaptation of bilby_pipe.job_creation.bilby_pipe_dag_creator.py
"""

import copy
from bilby_pipe.job_creation.node import Node
from bilby_pipe.utils import BilbyPipeError, get_colored_string, logger
from bilby_pipe.job_creation.dag import Dag
from bilby_pipe.job_creation.overview import create_overview
from bilby_pipe.job_creation.nodes import (
    FinalResultNode,
    MergeNode,
    PESummaryNode,
    GenerationNode,
    AnalysisNode,
    PlotNode,
    PostProcessAllResultsNode,
    PostProcessSingleResultsNode,
)
from golum_pipe.core.utils import (golum_logger, golum_logger_img2, 
                                    symmetric_golum_logger,
                                    joint_pe_golum_logger)

from . import generation_utils
from .nodes import (GolumImage2GenerationNode, GolumImage2AnalysisNode,
                    GolumPlotNode, BayesTypeIINode,
                    ImagePairReweightingNode, 
                    CoherenceRationComputationNode,
                    GolumRunsNode, GolumJointAnalysisNode,
                    JointCoherenceRatioComputationNode,
                    JointBayesFactorNode,
                    GolumBayesFactorNode)


def get_trigger_time_list(inputs):
    """Returns a list of GPS trigger times for each data segment"""
    if (inputs.gaussian_noise or inputs.zero_noise) and inputs.trigger_time is None:
        trigger_times = [0] * inputs.n_simulation
    elif (inputs.gaussian_noise or inputs.zero_noise) and isinstance(
        inputs.trigger_time, float
    ):
        trigger_times = [inputs.trigger_time] * inputs.n_simulation
    elif inputs.trigger_time is not None:
        trigger_times = [inputs.trigger_time]
    elif getattr(inputs, "gpstimes", None) is not None:
        start_times = inputs.gpstimes
        trigger_times = start_times + inputs.duration - inputs.post_trigger_duration
    else:
        raise BilbyPipeError("Unable to determine input trigger times from ini file")
    logger.info(f"Setting segment trigger-times {trigger_times}")
    return trigger_times


def get_detectors_list(inputs):
    detectors_list = []
    detectors_list.append(inputs.detectors)
    if inputs.coherence_test and len(inputs.detectors) > 1:
        for detector in inputs.detectors:
            detectors_list.append([detector])
    return detectors_list


def get_parallel_list(inputs):
    if inputs.n_parallel == 1:
        return [""]
    else:
        return [f"par{idx}" for idx in range(inputs.n_parallel)]


def generate_dag_second_image(inputs, ini_file, config):
    """
    Core logic setting up parent-child structure between nodes
    for the second image run
    """
    inputs = copy.deepcopy(inputs)
    dag = Dag(inputs)
    trigger_times = get_trigger_time_list(inputs)

    # Iterate over all generation nodes and store them in a list
    generation_node_list = []
    for idx, trigger_time in enumerate(trigger_times):
        kwargs = dict(trigger_time=trigger_time, idx=idx, dag=dag)
        if idx > 0:
            # Make all generation nodes depend on the 0th generation node
            # Ensures any cached files (e.g. the distance-marginalization
            # lookup table) are only built once.
            kwargs["parent"] = generation_node_list[0]
        generation_node = GolumImage2GenerationNode(inputs, **kwargs)
        generation_node_list.append(generation_node)

    detectors_list = get_detectors_list(inputs)
    parallel_list = get_parallel_list(inputs)
    merged_node_list = []
    all_parallel_node_list = []
    for generation_node in generation_node_list:
        for detectors in detectors_list:
            parallel_node_list = []
            for parallel_idx in parallel_list:
                analysis_node = GolumImage2AnalysisNode(
                    inputs,
                    generation_node=generation_node,
                    detectors=detectors,
                    parallel_idx=parallel_idx,
                    dag=dag,
                    sampler=inputs.sampler,
                )
                parallel_node_list.append(analysis_node)
                all_parallel_node_list.append(analysis_node)

            if len(parallel_node_list) == 1:
                merged_node_list.append(analysis_node)
            else:
                merge_node = MergeNode(
                    inputs=inputs,
                    parallel_node_list=parallel_node_list,
                    detectors=detectors,
                    dag=dag,
                )
                merged_node_list.append(merge_node)

    plot_nodes_list = []
    # check whether we want to perform the reweighting
    # and the coherence ratio computation
    # also check selection effects
    jp_reweighting = 'joint-posterior-reweighting=True'
    clu_computation = 'compute-coherence-ratio=False'
    selection_effects = 'selection-effects=False'
    for s in inputs.unknown_args:
        if 'joint-posterior-reweighting' in s:
            jp_reweighting = s
        elif 'compute-coherence-ratio' in s:
            clu_computation = s
        elif 'selection-effects' in s:
            selection_effects = s

    jp_reweighting = jp_reweighting.split('=')[-1]
    clu_computation = clu_computation.split('=')[-1]
    selection_effects = selection_effects.split('=')[-1]
    for merged_node in merged_node_list:
        if inputs.final_result:
            FinalResultNode(inputs, merged_node, dag=dag)
        if inputs.plot_node_needed:
            plot_nodes_list.append(GolumPlotNode(inputs, merged_node, dag=dag))
        if inputs.single_postprocessing_executable:
            PostProcessSingleResultsNode(inputs, merged_node, dag=dag)
        if jp_reweighting == 'True':
            golum_logger_img2.info("Computing the reweighted posteriors after the run")
            reweight_node = ImagePairReweightingNode(inputs, merged_node, dag = dag)
        if clu_computation == 'True':
            golum_logger_img2.info("Computing the Coherence ratio after the run")
            CoherenceRationComputationNode(inputs, merged_node, dag = dag)
        if selection_effects == 'True':
            golum_logger_img2.info("Computing the Bayes factor")
            GolumBayesFactorNode(inputs, reweight_node, ini_file, config, dag = dag)


    if inputs.create_summary:
        PESummaryNode(inputs, merged_node_list, generation_node_list, dag=dag)
    if inputs.postprocessing_executable is not None:
        PostProcessAllResultsNode(inputs, merged_node_list, dag)
    
    THRESHOLD = 21
    npar = len(all_parallel_node_list)
    if npar > THRESHOLD and inputs.osg is False:
        msg = (
            f"You are requesting {npar} analysis jobs, this work would be "
            "better suited to the IGWN-grid. See "
            "https://lscsoft.docs.ligo.org/bilby_pipe/master/osg.html "
            "for further information"
        )
        logger.warning(get_colored_string(msg))

    dag.build()
    create_overview(
        inputs,
        generation_node_list,
        all_parallel_node_list,
        merged_node_list,
        plot_nodes_list,
    )


def generate_dag_first_image(inputs):
    """
    Core logic setting up parent-child structure between nodes
    for the first image. 
    Had to be adapted to suit the correct plotting logic
    """
    inputs = copy.deepcopy(inputs)
    dag = Dag(inputs)
    trigger_times = get_trigger_time_list(inputs)

    # Iterate over all generation nodes and store them in a list
    generation_node_list = []
    for idx, trigger_time in enumerate(trigger_times):
        kwargs = dict(trigger_time=trigger_time, idx=idx, dag=dag)
        if idx > 0:
            # Make all generation nodes depend on the 0th generation node
            # Ensures any cached files (e.g. the distance-marginalization
            # lookup table) are only built once.
            kwargs["parent"] = generation_node_list[0]
        generation_node = GenerationNode(inputs, **kwargs)
        generation_node_list.append(generation_node)

    detectors_list = get_detectors_list(inputs)
    parallel_list = get_parallel_list(inputs)
    merged_node_list = []
    all_parallel_node_list = []
    for generation_node in generation_node_list:
        for detectors in detectors_list:
            parallel_node_list = []
            for parallel_idx in parallel_list:
                analysis_node = AnalysisNode(
                    inputs,
                    generation_node=generation_node,
                    detectors=detectors,
                    parallel_idx=parallel_idx,
                    dag=dag,
                    sampler=inputs.sampler,
                )
                parallel_node_list.append(analysis_node)
                all_parallel_node_list.append(analysis_node)

            if len(parallel_node_list) == 1:
                merged_node_list.append(analysis_node)
            else:
                merge_node = MergeNode(
                    inputs=inputs,
                    parallel_node_list=parallel_node_list,
                    detectors=detectors,
                    dag=dag,
                )
                merged_node_list.append(merge_node)

    plot_nodes_list = []
    
    # check if one wants to have the type II case
    typeII_stat = 'bayes-factor-type-2=False'
    for s in inputs.unknown_args:
        if 'bayes' in s and 'type-2' in s:
            typeII_stat = s
    typeII_stat = typeII_stat.split('=')[-1]
    for merged_node in merged_node_list:
        if inputs.final_result:
            FinalResultNode(inputs, merged_node, dag=dag)
        if inputs.plot_node_needed:
            plot_nodes_list.append(GolumPlotNode(inputs, merged_node, dag=dag))
        if inputs.single_postprocessing_executable:
            PostProcessSingleResultsNode(inputs, merged_node, dag=dag)
        # add the generation of the reweighing job
        if typeII_stat == 'True':
            logger.info("Bayes factor for type II images has been requested")
            BayesTypeIINode(inputs, merged_node, dag = dag)

    if inputs.create_summary:
        PESummaryNode(inputs, merged_node_list, generation_node_list, dag=dag)
    if inputs.postprocessing_executable is not None:
        PostProcessAllResultsNode(inputs, merged_node_list, dag)

    THRESHOLD = 21
    npar = len(all_parallel_node_list)
    if npar > THRESHOLD and inputs.osg is False:
        msg = (
            f"You are requesting {npar} analysis jobs, this work would be "
            "better suited to the IGWN-grid. See "
            "https://lscsoft.docs.ligo.org/bilby_pipe/master/osg.html "
            "for further information"
        )
        logger.warning(get_colored_string(msg))

    dag.build()
    create_overview(
        inputs,
        generation_node_list,
        all_parallel_node_list,
        merged_node_list,
        plot_nodes_list,
    )


def generate_dag_symmetric(args, config, output_directories, dag_img1img2, dag_img2img1):
    """
    Function making a dag combing the individual golum dags
    and those needed for Symmetric runs in one general dag

    ARGUMENTS:
    ----------
    - config: the initial symmetric config
    - output_directories: the output directories, storing where
                          the submit files should be stored
    - dag_img1img2: the dag file for the img1 - img2 golum run
    - dag_img2img1: the dag file for the img2 - img1 golum run

    RETURNS:
    --------
    - final_dag: the final dag combining the dags useful to run 
                 the symmetric analysis 
    """
    label = config.get('output_settings', 'label')
    subdir = output_directories['submit']
    final_dag_file = f'{subdir}/symmetric_golum_{label}.dag'

    parent_child = []

    # read in the config to see if we want to do the reweighting
    reweighting = config.getboolean('golum_settings', 'joint-posterior-reweighting', fallback = False)
    
    # check if the coherence ratio should be computed
    clu_computation = config.getboolean('golum_settings', 'compute-coherence-ratio', fallback = False)
    
    # check if selection effects are desired
    selection_effects = config.getboolean('population_settings', 'selection-effects', fallback = False)


    with open(final_dag_file, 'w', encoding = 'utf-8') as dag:
        dag.write(f'SUBDAG EXTERNAL golum_img1img2 {dag_img1img2} \n')
        dag.write(f'SUBDAG EXTERNAL golum_img2img1 {dag_img2img1} \n')
        
        if reweighting:
            generation_utils.setup_symmetric_reweighting(args, config, output_directories, dag)
            parent_child.append(f'Parent golum_img1img2 golum_img2img1 Child symmetric_reweighting_{label} \n')

        if clu_computation:
            generation_utils.setup_symmetric_clu_reweighting(args, config, output_directories, dag)
            parent_child.append(f'Parent golum_img1img2 golum_img2img1 Child symmetric_compute_clu_{label} \n')

        if selection_effects:
            generation_utils.setup_population_reweighting('symmetric', args, config, output_directories, dag)
            parent_child.append(f'Parent symmetric_reweighting_{label} symmetric_compute_clu_{label} Child symmetric_selection_effects_{label} \n')

            
        if len(parent_child) > 0:
            dag.write("\n\n")
            dag.write('#Inter-job dependencies \n')
            for par_child in parent_child:
                dag.write(par_child)



    symmetric_golum_logger.info(f'Symmetric dag file generated: {final_dag_file}')

    return final_dag_file


def generate_joint_dag(inputs_img1, inputs_img2, inputs_joint, args, config):
    """
    Script to generate the joint dag. It sets up the generation for each
    of the images using the usual generation process but for the different
    inputs. Then, the analysis job is set up by passing as arguments the 
    ini files for each image individually. We also need to pass the joint 
    input in order to have the labels.

    The analysis will require a new executable (call golum_joint_pe_analysis)

    ARGUMENTS:
    ----------
    - inputs_img1: the bilby input format for the first image
    - inputs_img2: the bilby input format for the second image
    - inputs_joint: the bilby input format for the joint run. This has dummy arguments
                   and is mostly used to get consistent labels when setting the run up
    - args: the arguments passed to the run, useful to get the run
    - config: useful to get information about the population part of the run
    """

    inputs_img1 = copy.deepcopy(inputs_img1)
    inputs_img2 = copy.deepcopy(inputs_img2)
    inputs_joint = copy.deepcopy(inputs_joint)


    # make one dag corresponding to the joint job
    dag = Dag(inputs_joint)

    # make the generation node for the first image
    trigger_times_img1 = get_trigger_time_list(inputs_img1)
    trigger_times_img2 = get_trigger_time_list(inputs_img2)

    generation_node_list_img1 = []
    for idx, trigger_time in enumerate(trigger_times_img1):
        kwargs = dict(trigger_time=trigger_time, idx=idx, dag=dag)
        if idx > 0:
            # Make all generation nodes depend on the 0th generation node
            # Ensures any cached files (e.g. the distance-marginalization
            # lookup table) are only built once.
            kwargs["parent"] = generation_node_list_img1[0]
        generation_node = GenerationNode(inputs_img1, **kwargs)
        generation_node_list_img1.append(generation_node)

    # make the generation of the second image as a child job of 
    # the generation of the first. This is not necessary but is easier
    # to comply to bilby_pipe
    generation_node_list_img2 = []
    for idx, trigger_time in enumerate(trigger_times_img2):
        kwargs = dict(trigger_time=trigger_time, idx=idx, dag=dag)
        kwargs['parent'] = generation_node_list_img1[idx]
        generation_node = GenerationNode(inputs_img2, **kwargs)
        generation_node_list_img2.append(generation_node)

    # setup the analysis node. It should be parent of the 
    # second image generation 
    all_parallel_node_list = []
    parallel_list= get_parallel_list(inputs_joint)
    merged_node_list = []
    for i in range(len(generation_node_list_img1)):
        parallel_node_list = []
        for parallel_idx in parallel_list:
            # do the generation
            analysis_node = GolumJointAnalysisNode(inputs_joint, generation_node_list_img1[i],
                                                   generation_node_list_img2[i],
                                                   parallel_idx = parallel_idx,
                                                   sampler = inputs_joint.sampler,
                                                   dag = dag,
                                                   img1_ini = inputs_img1.complete_ini_file,
                                                   img2_ini = inputs_img2.complete_ini_file)

            parallel_node_list.append(analysis_node)
            all_parallel_node_list.append(analysis_node)
        if len(parallel_node_list) == 1:
            merged_node_list.append(analysis_node)
        else:
            merge_node = MergeNode(inputs = inputs_joint,
                                   parallel_node_list = parallel_node_list,
                                   detectors = None,
                                   dag = dag)
            merged_node_list.append(merge_node)

    plot_nodes_list = []
    # check if the coherence ratio computation is needed
    clu_computation = 'compute-coherence-ratio=False'
    for s in inputs_joint.unknown_args:
        if 'compute-coherence-ratio' in s:
            clu_computation = s
    clu_computation = clu_computation.split("=")[-1]

    # check if selection effects should be accounted for
    selection_effects = 'selection-effects=False'
    for s in inputs_joint.unknown_args:
        if 'selection-effects' in s:
            selection_effects = s
    selection_effects = selection_effects.split('=')[-1]

    for merged_node in merged_node_list:
        if inputs_joint.final_result:
            FinalResultNode(inputs_joint, merged_node, dag=dag)
        if inputs_joint.plot_node_needed:
            plot_nodes_list.append(GolumPlotNode(inputs_joint, merged_node, dag=dag))
        if inputs_joint.single_postprocessing_executable:
            PostProcessSingleResultsNode(inputs_joint, merged_node, dag = dag)
        if clu_computation == 'True':
            joint_pe_golum_logger.info("Computing the coherence ratio after the run")
            joint_clu_node = JointCoherenceRatioComputationNode(inputs_joint, merged_node, config, dag = dag)
        if selection_effects == 'True':
            joint_pe_golum_logger.info('Computing the selection effects after the run')
            JointBayesFactorNode(inputs_joint, joint_clu_node, args.ini, config, dag = dag)

    if inputs_joint.create_summary:
        PESummaryNode(inputs_joint, merged_node_list, generation_node_list_img1,
                      generation_node_list_img2, dag) 
    if inputs_joint.postprocessing_executable is not None:
        PostProcessAllResultsNode(inputs_joint, merged_node_list, dag)

    THRESHOLD = 21
    npar = len(all_parallel_node_list)
    if npar > THRESHOLD and inputs.osg is False:
        msg = (
            f"You are requesting {npar} analysis jobs, this work would be "
            "better suited to the IGWN-grid. See "
            "https://lscsoft.docs.ligo.org/bilby_pipe/master/osg.html "
            "for further information"
        )
        logger.warning(get_colored_string(msg))

    dag.build()
    create_overview(
        inputs_joint,
        [generation_node_list_img1, generation_node_list_img2],
        all_parallel_node_list,
        merged_node_list,
        plot_nodes_list,
    )
