"""
Utilities to take care of slurm submit file generation.
This is partially based on bilby_pipe.job_creation.slurm.py
"""

import os
from golum_pipe.core.utils import symmetric_golum_logger

def build_slurm_submit_symmetric(args, config, output_directories):
    """
    Function making the slurm submit files for symmetric runs.

    This generates one master bash file
    and several individual slurm files for the post-processing jobs.
    The slurm files for the main img1img2 and img2img1 jobs
    should have already been produced by bilby_pipe.

    ARGUMENTS:
    ----------
    - args: the overall parsed arguments of the golum_symmetric executable
    - config: the initial symmetric config
    - output_directories: the output directories, storing where
                          the submit files should be stored

    RETURNS:
    --------
    - slurm_master_file: name of the final bash submit file
    """
    label = config.get('output_settings', 'label')
    subdir = output_directories['submit']
    slurm_master_file = f'{subdir}/bash_{label}_slurm_master.sh'
    slurm_id_file = f'{subdir}/slurm_ids'
    symmetric_golum_logger.info(f'Building slurm master submit file: {slurm_master_file}')

    # read and insert the contents of the main sampling master submit scripts
    # this is necessary because we don't know how to do job dependencies across multiple master files
    lines_imgruns = {}
    pair_run_last_jobs = []
    for pair in ['img1img2', 'img2img1']:
        infile = f'{subdir}/slurm_{label}{pair}_master.sh'
        if not os.path.isfile(infile):
            raise IOError(f'Missing file {infile} that should have been created by bilby_pipe.')
        symmetric_golum_logger.info(f'Including contents from file: {infile}')
        with open(infile, 'r') as f:
            lines_imgruns[pair] = f.readlines()
            lines_imgruns[pair] = [l.replace('jid',f'jid{pair}_') for l in lines_imgruns[pair] if not l.startswith('#')]
        pair_run_last_jobs.append([l for l in lines_imgruns[pair] if l.startswith('jid')][-1].split('=')[0])

    # check if we want to do the reweighting
    reweighting = config.getboolean('golum_settings', 'joint-posterior-reweighting', fallback = False)

    # check if the coherence ratio should be computed
    clu_computation = config.getboolean('golum_settings', 'compute-coherence-ratio', fallback = False)

    # check if selection effects are desired
    selection_effects = config.getboolean('population_settings', 'selection-effects', fallback = False)

    # build dict for extra postproc jobs
    jobs = {}

    if reweighting:
        job = f'{label}_symmetric_reweight'
        jobs[job] = {}
        jobs[job]['executable'] = 'golum_symmetric_reweight'
        jobs[job]['args'] = f'--ini-file {args.ini}'
        jobs[job]['dependencies'] = pair_run_last_jobs

    if clu_computation:
        job = f'{label}_symmetric_compute_clu'
        jobs[job] = {}
        jobs[job]['executable'] = 'golum_symmetric_clu_computation'
        jobs[job]['args'] = f'--ini-file {args.ini}'
        jobs[job]['dependencies'] = pair_run_last_jobs

    if selection_effects:
        job = f'{label}_symmetric_compute_bayes_factor'
        jobs[job] = {}
        jobs[job]['executable'] = 'golum_compute_bayes_factor'
        jobs[job]['args'] = f'--ini-file {args.ini} --case symmetric'
        jobs[job]['dependencies'] = [job for job in jobs if 'reweight' in job or 'clu' in job]

    # assign new job ID to each process
    for num, job in enumerate(jobs):
        jobs[job]['jid'] = f'sym_{num}'

    with open(slurm_master_file, 'w', encoding = 'utf-8') as f:
        f.write('#!/bin/bash\n')

        for lines in lines_imgruns.values():
            for l in lines:
                f.write(l)
            f.write('\n')

        for job in jobs:
            jid  = jobs[job]['jid']
            submit_str = f'\njid{jid}=($(sbatch --job-name={job}'

            if len(jobs[job]['dependencies']) > 0:
                # only run subsequent jobs after parent has
                # *successfully* completed
                submit_str += ' --dependency=afterok'
                for parent in jobs[job]['dependencies']:
                    if parent.startswith('jid'):
                        submit_str += f':${{{parent}[-1]}}'
                    else:
                        submit_str += f':${{jid{jobs[parent]["jid"]}[-1]}}'

            job_script = _write_individual_processes_symmetric(config, output_directories, job, jobs[job]['executable'], jobs[job]['args'])

            submit_str += f' {job_script}))\n\n'
            submit_str += (
                f'echo "jid{jid} ${{jid{jid}[-1]}}" >> {slurm_id_file}'
            )

            f.write(f'{submit_str}\n')

    return slurm_master_file


def _write_individual_processes_symmetric(config, output_directories, name, executable, args):
    fname = f'slurm_{name}.sh'
    job_path = f'{output_directories["submit"]}/{fname}'

    # collect overall and postproc-specific slurm options in a dict
    scheduler_args = config.get('condor_settings','scheduler-args', fallback=None)
    if scheduler_args is not None:
        slurm_args = {arg.split('=')[0]: arg.split('=')[1] for arg in scheduler_args.split()}
    else:
        slurm_args = {}
    slurm_args['nodes'] = 1
    slurm_args['ntasks-per-node'] = 1
    slurm_args['mem'] = f'{int(float(config.get("condor_settings","request-memory", fallback="1GB").rstrip("GB")))}G'
    slurm_args['time'] = config.get('condor_settings','scheduler-analysis-time', fallback='01:00:00')
    slurm_args['job-name'] = name

    with open(job_path, 'w') as f:
        f.write('#!/bin/bash\n')

        # get output and error file paths
        output_file = f'{output_directories["outdir"]}/log_data_analysis/{name}.out'
        slurm_args['output'] = output_file
        slurm_args['error'] = output_file.replace('.out', '.err')

        for key, val in slurm_args.items():
            f.write(f'#SBATCH --{key}={val}\n')

        scheduler_modules = config.get('condor_settings','scheduler-modules', fallback=None)
        if scheduler_modules:
            for module in scheduler_modules:
                if module is not None:
                    f.write(f'\nmodule load {module}\n')

        scheduler_env = config.get('condor_settings','scheduler-env', fallback=None)
        if scheduler_env is not None:
           f.write('\nsource {}\n'.format(scheduler_env))

        f.write('\n')

        job_str = f'{executable} {args}\n'
        f.write(job_str)

    return job_path
