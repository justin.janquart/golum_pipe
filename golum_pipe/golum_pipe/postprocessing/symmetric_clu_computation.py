"""
script actually computing the coherence ratio
in the symmetric case.
"""


import os
import bilby
import sys
import json
import numpy as np
from configparser import ConfigParser
from bilby_pipe.bilbyargparser import BilbyArgParser
from bilby_pipe.utils import DataDump
from bilby_pipe.main import parse_args
from golum_pipe.core.utils import symmetric_golum_logger
from golum.tools import postprocessing
from golum.tools import utils as golumutils
import scipy.special

def create_reweighting_parser():
    """
    Create a parser adapted to the reweighting
    """
    parser = BilbyArgParser(ignore_unknown_config_file_keys = True)
    parser.add("--ini-file", required = True, help = "The initial ini file to read the reweighting config")

    return parser


def compute_coherence_ratio(args, unknown_args):
    """
    Function computing the coherence ratio

    ARGUMENTS:
    ----------
    - args: arguments known to the parser
    - unknown_args: the arguments no known to the parser
    """

    # read in the ini file 
    config = ConfigParser(converters = {'dict' : lambda x: ast.literal_eval(x)})

    if not os.path.isfile(args.ini_file):
        raise IOError(f"{args.ini_file} does not exist")

    try:
        config.read(args.ini_file)
    except IOError:
        raise ValueError(f"Cannot read file {args.ini_file}")

    outdir = config.get('output_settings', 'outdir')
    label = config.get('output_settings', 'label')
    img1_lensed_file = config.get('golum_settings', 'posterior-file-image-1')
    img2_lensed_file = config.get('golum_settings', 'posterior-file-image-2')
    img1_unlensed_file = config.get('golum_settings', 'unlensed-file-image-1', fallback = None)
    img2_unlensed_file = config.get('golum_settings', 'unlensed-file-image-2', fallback = None)

    # find the two results files
    result_dir = f'{outdir}/final_result/'
    data_dir = f'{outdir}/data/'

    result_file_img1img2 = None
    result_file_img2img1 = None
    for file in os.listdir(result_dir):
        if 'img1img2' in file and '_result' in file:
            result_file_img1img2 = file
        elif 'img2img1' in file and '_result' in file:
            result_file_img2img1 = file
    if result_file_img2img1 is None:
        raise ValueError(f'Unable to locate result file "{result_file_img2img1}".')
    if result_file_img1img2 is None:
        raise ValueError(f'Unable to locate result file "{result_file_img1img2}".')

    # read in the results and get the log evidence
    log_evidence_img1img2 = bilby.result.read_in_result(filename = '%s%s'%(result_dir, result_file_img1img2)).log_evidence
    log_evidence_img2img1 = bilby.result.read_in_result(filename = '%s%s'%(result_dir, result_file_img2img1)).log_evidence
    log_evidence_img1_lens = bilby.result.read_in_result(filename = img1_lensed_file).log_evidence
    log_evidence_img2_lens = bilby.result.read_in_result(filename = img2_lensed_file).log_evidence

    # check in which conditions we are 
    if img1_unlensed_file is None or img2_unlensed_file is None:
        symmetric_golum_logger.info("No unlensed result file specified, computing approximate coherence ratio")
        part_1 = log_evidence_img1img2-log_evidence_img2_lens
        part_2 = log_evidence_img2img1-log_evidence_img1_lens
        log_clu = 0.5*scipy.special.logsumexp([part_1, part_2])
    else:
        symmetric_golum_logger.info("Computing the approximate coherence ratio")
        log_evidence_img1_unlens = bilby.result.read_in_result(filename = img1_unlensed_file).log_evidence
        log_evidence_img2_unlens = bilby.result.read_in_result(filename = img2_unlensed_file).log_evidence
        part_1 = log_evidence_img1img2+log_evidence_img1_lens-log_evidence_img1_unlens-log_evidence_img2_unlens
        part_2 = log_evidence_img2img1+log_evidence_img2_lens-log_evidence_img1_unlens-log_evidence_img2_unlens
        log_clu = 0.5*scipy.special.logsumexp([part_1, part_2])


    # write the result in a file
    with open(f'{result_dir}{label}_log_clu.txt', 'w') as fout:
        fout.write(str(log_clu))

    symmetric_golum_logger.info(f" log(Clu) = {log_clu:.3f}")



def main():
    """
    top level interface for the computation
    """
    # parse the ini file given as arguments
    args, unknown_args = parse_args(sys.argv[1:], create_reweighting_parser())
    compute_coherence_ratio(args, unknown_args)
