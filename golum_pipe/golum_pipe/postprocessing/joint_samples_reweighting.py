"""
Script performing the sample reweighting after the
usual GOLUM run has been done
"""

import os
import bilby 
import json
import sys
import numpy as np
from bilby.gw.result import CBCResult
from importlib import import_module
from bilby_pipe.bilbyargparser import BilbyArgParser
from golum_pipe.core.utils import golum_logger_img2, make_waveform_generator_from_result_object
from bilby_pipe.utils import parse_args, get_command_line_arguments, DataDump
from golum.tools import utils as golumutils
from golum.pe import likelihood as lens_likeli

def create_reweighting_parser():
    """
    Create a parser adapted for reweighting
    """
    parser = BilbyArgParser(ignore_unknown_config_file_keys = True)
    parser.add("--result", type=str, required = True, help="The result file")
    parser.add("--posterior-file-image-1", type = str, required  = True, help = 'File with the posteriors for the first image')
    parser.add(
        "--outdir", type=str, required = False, help="The directory to save the plots in"
    )
    parser.add("--n-samples", type = int, required = True, help = 'Number of samples used to make the lookup table')
    parser.add("--use-effective-parameters", type = str, default = 'False', help = 'Whether we are using the effective parameters or not')
    parser.add("--use-prior-for-look-up", type = bool, required = True, help = 'Whether the prior have been used for the likelihood or not')
    parser.add("--sample-seed", type = str, required = True, help = "Seed used when sampling the first image samples")
    parser.add('--n-samples-reweighting', type = int, default = int(1e5), help = 'number of samples to use in reweighting process')
    
    return parser

def reweight_joint_samples(args, unknown_args):
    """
    Function charging the golum results and computing the 
    reweighting. The reweighted samples are save in the 
    result file

    ARGUMENTS:
    ----------
    - args: the arguments parsed properly, should have both
            the result and img1 posterior files
    - unknown args: all the other unrecognized arguments
    """
    
    if hasattr(args, "webdir"):
        outdir = os.path.join(args.webdir, "bilby")
    elif hasattr(args, "outdir"):
        outdir = args.outdir
    else:
        outdir = result.outdir

    golum_logger_img2.info(f"computing the joint posteriors and saving them in {args.outdir}")

    # read in the results from the golum run
    extension = os.path.splitext(args.result)[-1][1:]
    if extension == "json":
        result_img2 = CBCResult.from_json(args.result)
    elif extension == "hdf5":
        result_img2 = CBCResult.from_hdf5(args.result)
    elif extension == "pkl":
        result_img2 = CBCResult.from_pickle(args.result)
    else:
        raise ValueError(f"Result format {extension} not understood.")

    # load the results from the first image run
    golum_logger_img2.info(f"arg post file {args.posterior_file_image_1}")
    _, img1_posteriors = golumutils.read_image1_file(args.posterior_file_image_1)

    # reweighting requires to have the likelihood setup
    # can be reconstructed from results
    n_samps = args.n_samples
    golum_logger_img2.info(f"arg n_samps {args.n_samples}")
    use_effective_parameters = args.use_effective_parameters
    if use_effective_parameters == 'True':
        use_effective_params = True
    else:
        use_effective_params = False
    golum_logger_img2.info(f"arg eff params {args.use_effective_parameters}")
    use_prior_for_lookup = args.use_prior_for_look_up
    sample_seed = args.sample_seed
    if sample_seed == 'None':
        sample_seed = None
    else:
        try:
            sample_seed = int(sample_seed)
        except:
            # if for some reason this does not work, just drop 
            # the value of the seed
            sample_seed = None

    waveform_generator = make_waveform_generator_from_result_object(result_img2)

    # first try to read in the data_dump file
    if result_img2.meta_data['command_line_args']['data_dump_file'] is not None:
        golum_logger_img2.info('Using data dump file for ifos')
        _data_dump = DataDump(result_img2.meta_data['command_line_args']['data_dump_file'])
        ifos = _data_dump.interferometers

    # try to reconstruct the path to data dump file
    # sometimes not saved properly 
    elif os.path.isfile(f"{result_img2.meta_data['command_line_args']['outdir']}/{result_img2.meta_data['command_line_args']['label']}_data_dump.pickle"):
        golum_logger_img2.info('Found data dump file even if not properly saved, using it')
        _data_dump = DataDump(f"{result_img2.meta_data['command_line_args']['outdir']}/{result_img2.meta_data['command_line_args']['label']}_data_dump.pickle")
        ifos = _data_dump.interferometers
    
    else:
        
        img2_ifos_list = []
        for det in result_img2.meta_data['command_line_args']['detectors']:
            print(det)
            if "'" in det:
                new_det = det.replace("'", "")
            else:
                new_det = det
            img2_ifos_list.append(new_det)
        
        ifos = bilby.gw.detector.InterferometerList(img2_ifos_list)
        # need to setup the interferometers 
        if result_img2.meta_data['command_line_args']['zero_noise'] == True:
            start_time = float(result_img2.meta_data['command_line_args']['trigger_time']) \
                         + float(result_img2.meta_data['command_line_args']['post_trigger_duration']) \
                         - result_img2.meta_data['command_line_args']['duration']
            if result_img2.meta_data['command_line_args']['injection_file'] is not None:
                data_inj = np.genfromtxt(result_img2.meta_data['command_line_args']['injection_file'], names = True)
                injection_parameters = dict()
                for key in data_inj.dtype.names:
                    injection_parameters[key] = data_inj[key] # assuming one injection
            elif result_img2.meta_data['command_line_args']['injection_dict'] is not None:
                injection_parameters = result_img2.meta_data['command_line_args']['injection_dict'].copy()

            else:
                try:
                    injection_parameters = result_img2.injection_parameters.copy()
                except:
                    raise ValueError("Impossible to recover the injection parameters")
        
            # set the result for no noise
            golum_logger_img2.info("Reconstructing zero noise injection")
            if result_img2.meta_data['command_line_args']['injection'] == False:
                # noise only
                ifos.set_strain_data_from_zero_noise(sampling_frequency = result_img2.meta_data['command_line_args']['sampling_frequency'],
                                                     duration = result_img2.meta_data['command_line_args']['duration'],
                                                     start_time = start_time)
            else:
                # also add an event
                ifos.set_strain_data_from_zero_noise(sampling_frequency = result_img2.meta_data['command_line_args']['sampling_frequency'],
                                                     duration = result_img2.meta_data['command_line_args']['duration'],
                                                     start_time = start_time)
                ifos.inject_signal(waveform_generator = waveform_generator,
                                   parameters = injection_parameters)

        elif result_img2.meta_data['command_line_args']['gaussian_noise'] == True:
            golum_logger_img2.info("Reconstructing gaussian noise injection")
            
            # fetch the injection parameters
            start_time = float(result_img2.meta_data['command_line_args']['trigger_time']) \
                         + float(result_img2.meta_data['command_line_args']['post_trigger_duration']) \
                         - result_img2.meta_data['command_line_args']['duration']
            if result_img2.meta_data['command_line_args']['injection_file'] is not None:
                data_inj = np.genfromtxt(result_img2.meta_data['command_line_args']['injection_file'], names = True)
                injection_parameters = dict()
                for key in data_inj.dtype.names:
                    injection_parameters[key] = data_inj[key] # assuming one injection
            elif result_img2.meta_data['command_line_args']['injection_dict'] is not None:
                injection_parameters = result_img2.meta_data['command_line_args']['injection_dict'].copy()

            else:
                try:
                    injection_parameters = result_img2.injection_parameters.copy()
                except:
                    raise ValueError("Impossible to recover the injection parameters")
        
            ifos.set_strain_data_from_power_spectral_densities(sampling_frequency = result_img2.meta_data['command_line_args']['sampling_frequency'],
                                                              duration = result_img2.meta_data['command_line_args']['duration'],
                                                              start_time = start_time)

            if result_img2.meta_data['command_line_args']['injection'] != False:
                ifos.inject_signal(waveform_generator = waveform_generator,
                               parameters = injection_parameters)

        else:
            # set from real data
            golum_logger_img2.info("Loading ifos from data")
            ifos = bilby.gw.detector.InterferometerList([])
            start_time = float(result_img2.meta_data['command_line_args']['trigger_time']) \
                         + float(result_img2.meta_data['command_line_args']['post_trigger_duration']) \
                         - result_img2.meta_data['command_line_args']['duration']
            roll_off = result_img2.meta_data['command_line_args']['tukey_roll_off']
            for det in result_img2.meta_data['command_line_args']['detectors']:
                ifo = bilby.gw.detector.get_empty_interferometer(det)
                ifo.strain_data.roll_off = roll_off

                if result_img2.meta_data['command_line_args']['psd_dict'] is not None and det in result_img2.meta_data['command_line_args']['psd_dict']:
                    psd_data = None
                    ifo.power_spectral_density = bilby.gw.detector.PowerSpectralDensity.from_power_spectral_density_file(psd_file = result_img2.meta_data['command_line_args']['psd_dict'][det])

                else:
                    # else, set the psd from the data
                    raise NotImplementedError("This case is not foreseen yet")

                channel = result_img2.meta_data['command_line_args']['channel_dict']
                if channel is not None:
                    ifo.set_strain_data_from_channel_name(channel,
                                                          sampling_frequency = result_img2.meta_data['command_line_args']['sampling_frequency'],
                                                          duration = result_img2.meta_data['command_line_args']['duration'],
                                                          start_time = start_time)
                else:
                    raise NotImplementedError("Data need to be read by channel or ifo from data_dump file")


                ifos.append(ifo)

    priors = result_img2.priors
    # need to add the time delay prior here (later, now just dropping)
    # now we can set the golum likelihood up 
    golum_logger_img2.info(f'use effective params: {use_effective_params}')
    # added for the case where we have no Morse factor for the first image
    if "n_phase" not in img1_posteriors:
        img1_posteriors['n_phase'] = [0 for i in range(len(img1_posteriors['geocent_time']))]
    golum_likelihood = lens_likeli.GravitationalWaveTransientImage2(ifos,
                                                                    waveform_generator = waveform_generator,
                                                                    posteriors = img1_posteriors,
                                                                    n_samp = n_samps,
                                                                    seed = sample_seed,
                                                                    priors = None,
                                                                    use_effective_parameters = use_effective_params)

    # get the reweighed samples as output
    n_samples_reweight = args.n_samples_reweighting

    outfile = f'{outdir}/{result_img2.label}_reweighted_samples.json'

    if os.path.isfile(outfile):
        golum_logger_img2.info(f'{outfile} already exists, no reweighting needed')

    else:
        bilby.core.utils.check_directory_exists_and_if_not_mkdir(outdir)

        if use_effective_params:
            out_dict = golumutils.reweighting_event_1_effective_parameters(result_img2, golum_likelihood,
                                                                           ifos, waveform_generator,
                                                                           n_samples_reweight,
                                                                           args.posterior_file_image_1, 2)
        else:
            out_dict = golumutils.reweighting_event_1(result_img2, golum_likelihood,
                                                      ifos, waveform_generator,
                                                      n_samples_reweight,
                                                      args.posterior_file_image_1, 2)

        golum_logger_img2.info(f"Saving the reweighted posteriors to {outfile}")
        
        # convert things to json compatible output format
        for key in out_dict.keys():
            for kk in out_dict[key]:
                out_dict[key][kk] = [float(out_dict[key][kk][i]) for i in range(len(out_dict[key][kk]))]
        with open(outfile, 'w') as fout:
            json.dump(out_dict, fout, indent = 4)



def main():
    """
    Top level interface to perform the reweighting
    """
    args, unknown_args = parse_args(sys.argv[1:], create_reweighting_parser())
    reweight_joint_samples(args, unknown_args)
