"""
Script where the clu computation actually happens
"""

import os
import bilby 
import json
import sys
from bilby_pipe.bilbyargparser import BilbyArgParser
from bilby_pipe.utils import parse_args
from golum_pipe.core.utils import joint_pe_golum_logger
from golum_pipe.postprocessing.pair_coherence_ratio_computation import standard_prior_setter
from golum.tools import conversion as golumconversion 


def create_clu_parser():
    """
    Create parser adapted for the joint coherence ratio 
    computation
    """

    parser = BilbyArgParser(ignore_unknown_config_file_keys = True)
    parser.add("--result", type=str, required = True, help="The result file")
    parser.add("--unlensed-file-image-1", type = str, default = 'None', help = 'Path to the unlensed result file for the first image')
    parser.add("--unlensed-file-image-2", type = str, default = 'None', help = 'Path to the unlensed result file for the second image')
    
    return parser

def compute_joint_coherence_ratio(args, unknown_args):
    """
    Function actually doing the job by computing the 
    coherence ratio and writing the result in an output file
    """
    result = bilby.result.read_in_result(filename = args.result)
    # setup the output directory
    if hasattr(args, "webdir"):
        outdir = os.path.join(args.webdir, "bilby")
    elif hasattr(args, "outdir"):
        outdir = args.outdir
    else:
        outdir = result.outdir

    img1_unl_file = args.unlensed_file_image_1
    img2_unl_file = args.unlensed_file_image_2
    if img1_unl_file == 'None' or img2_unl_file == 'None':
        raise ValueError("If the unlensed result files are not parsed, we cannot compute the coherence ratio")

    # change to standard priors
    std_prior = standard_prior_setter()
    logZ_img1_unl, _ = golumconversion.change_in_prior(img1_unl_file, std_prior)
    logZ_img2_unl, _ = golumconversion.change_in_prior(img2_unl_file, std_prior)

    # get the values for the lensed results
    joint_lens_logZ = result.log_evidence

    log_clu = joint_lens_logZ - logZ_img1_unl - logZ_img2_unl

    outfile = f'{outdir}/{result.label}_log_coherence_ratio.json'
    joint_pe_golum_logger.info(f"Saving the log coherence ratio in {outfile}")
    with open(outfile, 'w') as fout:
        json.dump({'log_clu' : float(log_clu)}, fout,
                  indent = 4)    

    joint_pe_golum_logger.info(f'We found a log coherence ratio of {float(log_clu)}')


def main():
    """
    Top level interface to perform the coherence ratio computation

    ARGUMENTS:
    ----------
    - args: the arguments known to the parser
    - unknown_args: the arguments not known to the sampler

    """

    args, unknown_args = parse_args(sys.argv[1:], create_clu_parser())
    compute_joint_coherence_ratio(args, unknown_args)
