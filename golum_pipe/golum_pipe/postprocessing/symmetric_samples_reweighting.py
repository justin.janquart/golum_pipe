"""
Script to actually perform the posterior reweighting when
one does a joint run 
"""

import os
import bilby
import json
import sys
import numpy as np
from configparser import ConfigParser
from bilby_pipe.bilbyargparser import BilbyArgParser
from bilby_pipe.utils import DataDump
from bilby_pipe.main import parse_args
from golum_pipe.core.utils import symmetric_golum_logger, make_waveform_generator_from_result_object
from golum.tools import postprocessing 
from golum.tools import utils as golumutils


def create_reweighting_parser():
    """
    Create a parser adapted to the reweighting
    """
    parser = BilbyArgParser(ignore_unknown_config_file_keys = True)
    parser.add("--ini-file", required = True, help = "The initial ini file to read the reweighting config")

    return parser

def reweight_symmetric_to_joint_samples(args, unknown_args):
    """
    Function to reweight the individual symmetric results to joint
    results.

    ARGUMENTS:
    ----------
    - args: arguments known to the parser
    - unknown_args: the arguments no known to the parser
    """

    # read in the ini file 
    config = ConfigParser(converters = {'dict' : lambda x: ast.literal_eval(x)})

    if not os.path.isfile(args.ini_file):
        raise IOError(f"{args.ini_file} does not exist")

    try:
        config.read(args.ini_file)
    except IOError:
        raise ValueError(f"Cannot read file {args.ini_file}")

    # fetch the data needed to do the reweighting
    n_samples_reweight = config.getint('golum_settings', 'n-samples-reweighting', fallback = int(1e5))
    outdir = config.get('output_settings', 'outdir')
    label = config.get('output_settings', 'label')
    img1_file = config.get('golum_settings', 'posterior-file-image-1')
    img2_file = config.get('golum_settings', 'posterior-file-image-2')

    # fetch the results for the run
    result_dir = f'{outdir}/final_result/'
    data_dir = f'{outdir}/data/'

    result_file_img1img2 = None
    result_file_img2img1 = None
    for file in os.listdir(result_dir):
        if 'img1img2' in file and '_result' in file:
            result_file_img1img2 = file
        elif 'img2img1' in file and '_result' in file:
            result_file_img2img1 = file
    if result_file_img2img1 is None:
        raise ValueError(f'Unable to locate result file "{result_file_img2img1}".')
    if result_file_img1img2 is None:
        raise ValueError(f'Unable to locate result files "{result_file_img1img2}".')

    # read in the data dump file
    data_dump_file_img1img2 = None
    data_dump_file_img2img1 = None
    for file in os.listdir(data_dir):
        if 'img1img2' in file and '_data_dump.pickle' in file:
            data_dump_file_img1img2 = file
        elif 'img2img1' in file and '_data_dump.pickle' in file:
            data_dump_file_img2img1 = file
    if data_dump_file_img1img2 is None or data_dump_file_img2img1 is None:
        raise ValueError("Unable to locate data dump files")

    # read in the results
    results_img1img2 = bilby.result.read_in_result(filename = '%s%s'%(result_dir, result_file_img1img2))
    results_img2img1 = bilby.result.read_in_result(filename = '%s%s'%(result_dir, result_file_img2img1))

    # read in the data dump and get the ifos
    _data_dump_img1img2 = DataDump.from_pickle('%s%s'%(data_dir, data_dump_file_img1img2))
    ifos_img2 = _data_dump_img1img2.interferometers
    _data_dump_img2img1 = DataDump.from_pickle('%s%s'%(data_dir, data_dump_file_img2img1))
    ifos_img1 = _data_dump_img2img1.interferometers

    # for efficiency, if events have the same duration and sampling frequency
    # we use a single waveform generator, else we set up two
    duration_img1img2 = results_img1img2.meta_data['command_line_args']['duration']
    duration_img2img1 = results_img2img1.meta_data['command_line_args']['duration']
    is_same_duration = duration_img1img2 == duration_img2img1

    sampling_frequency_img1img2 = results_img1img2.meta_data['command_line_args']['sampling_frequency']
    sampling_frequency_img2img1 = results_img2img1.meta_data['command_line_args']['sampling_frequency']
    is_same_sampling_frequency = sampling_frequency_img1img2 == sampling_frequency_img2img1
    
    # make the waveform generator
    waveform_generator = make_waveform_generator_from_result_object(results_img1img2)
    waveform_generator_image_2 = waveform_generator if (is_same_duration and is_same_sampling_frequency) else make_waveform_generator_from_result_object(results_img2img1)


    # get the posteriors for the 16D runs
    _, img1_posteriors = golumutils.read_image1_file(img1_file)
    _, img2_posteriors = golumutils.read_image1_file(img2_file)

    # !!! inversion in ordering definition to make sure we get
    # img1img2 like results (important to get same results in reweighting)
    reweighted_samples = postprocessing.reweight_symmetric_to_joint_samples(img2_posteriors, img1_posteriors,
                                                                            results_img2img1, results_img1img2, 
                                                                            ifos_img2, ifos_img1,
                                                                            waveform_generator,
                                                                            waveform_generator_image_2,
                                                                            n_points = n_samples_reweight)

    # save the samples 
    with open(f'{result_dir}{label}_joint_reweighted_samples.json', 'w') as fout:
        json.dump(reweighted_samples, fout, indent = 4)


def main():
    """
    Top level interface for the reweighting 
    """

    # parse the ini file given as arguments
    args, unknown_args = parse_args(sys.argv[1:], create_reweighting_parser())
    reweight_symmetric_to_joint_samples(args, unknown_args)
