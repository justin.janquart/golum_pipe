"""
Top level script for unlensed selection effects
"""

import re
import sys
from golum_pipe.population.selection_effects_unlensed import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())