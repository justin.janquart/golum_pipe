"""
Top level interface to compute the bayes factor
"""

import re
import sys
from golum_pipe.population.bayes_factor_computation import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())