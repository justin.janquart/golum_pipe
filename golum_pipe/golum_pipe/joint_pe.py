"""
Main script for the joint PE analysis
"""

import argparse
import bilby
import bilby_pipe
import subprocess
import shlex

from golum_pipe.core.utils import joint_pe_golum_logger
from golum_pipe.core import utils, golum_parsers, to_bilby_pipe
from golum_pipe.job_generation import dag_generation


def main():
    """
    Main function to setup the different analyses. 
    First we check the injections and setup the injection 
    files. 
    Then, we setup the joint PE analysis and post-processing step
    """

    golumparser = golum_parsers.set_golum_parser()
    args = golumparser.parse_args()

    # read in the ini
    config = utils.verify_config_file(args)

    # check whether we are dealing with injections or not
    if config.getboolean('injection_settings_image_1', 'injection'):
        args.injection_img1 = True
    else:
        args.injection_img1 = False
    if config.getboolean('injection_settings_image_2', 'injection'):
        args.injection_img2 = True
    else:
        args.injection_img2 = False
    if config.getboolean('condor_settings', 'local'):
        args.local = True
    else:
        args.local = False

    # setting up the output directories
    outdir = config.get('output_settings', 'outdir', fallback = ".")
    output_directories = {'outdir' : outdir, 'data' : f'{outdir}/data',
                          'submit' : f'{outdir}/submit'}
    label = config.get('output_settings', 'label', fallback = 'dummy_label')

    # create the directories
    for _, directory in output_directories.items():
        bilby.core.utils.check_directory_exists_and_if_not_mkdir(directory)

    # setup the logger 
    logging_level = config.get('output_settings', 'logging-level', fallback = 'INFO')
    utils.setup_logger(output_directories['outdir'], logging_level, args = args)
    joint_pe_golum_logger.info(f'Golum logger setup, log output in {outdir}/golum_joint_pe.log')

    
    # need to parse the arguments in a way that 2 files can be generated
    # in the generation part (ideal would be to just parse the initial ini file)
    joint_pe_golum_logger.info("Generating ini file for first image injection")
    img1_config = utils.make_golum_config_from_joint_pe(config, n_img = 1)
    with open(f'{outdir}/{label}_image1_golum.ini', 'w') as img1_ini:
        img1_config.write(img1_ini)

    img1_inifile = f'{outdir}/{label}_image1_golum.ini'

    # do the same for the second image
    joint_pe_golum_logger.info("Generating ini file for second image injection")
    img2_config = utils.make_golum_config_from_joint_pe(config, n_img = 2)
    with open(f'{outdir}/{label}_image2_golum.ini', 'w') as img2_ini:
        img2_config.write(img2_ini)

    img2_inifile = f'{outdir}/{label}_image2_golum.ini'
    


    # generate the input for each image. This will be passed 
    # to the usual generation process (like for the first image)
    # to make the data
    if args.injection_img1 or config.getboolean('injection_settings_image_1', 'injection'):
        joint_pe_golum_logger.info('Generating injection information for the first image')
        injection_wf_args_img1 = utils.make_waveform_arguments(config, n_img = 1, injection = True)
        joint_pe_golum_logger.info('Injection waveform arguments image 1: %s', injection_wf_args_img1)
        injection_file_img1 = utils.make_injection_file_for_joint(config, n_img = 1)
        joint_pe_golum_logger.info('Injection file for first image generated')
    else:
        injection_file_img1 = None
        injection_wf_args_img1 = None

    joint_pe_golum_logger.info("Generating analysis waveform argument for the first image")
    analysis_wf_arg_img1 = utils.make_waveform_arguments(img1_config)

    # same for the other image
    if args.injection_img2 or config.getboolean('injection_settings_image_2', 'injection'):
        joint_pe_golum_logger.info('Generating injection information for the second image')
        injection_wf_args_img2 = utils.make_waveform_arguments(config, n_img = 2, injection = True)
        joint_pe_golum_logger.info('Injection waveform arguments image 2: %s', injection_wf_args_img2)
        injection_file_img2 = utils.make_injection_file_for_joint(config, n_img = 2)
        joint_pe_golum_logger.info('Injection file for second image generated')
    else:
        injection_file_img2 = None
        injection_wf_args_img2 = None

    joint_pe_golum_logger.info("Generating analysis waveform argument for the second image")
    analysis_wf_arg_img2 = utils.make_waveform_arguments(img2_config)

    # generate the inputs for each of the images
    joint_pe_golum_logger.info("Generating input info for image 1")
    bp_config_img1 = to_bilby_pipe.make_bilby_pipe_config(img1_config, args, output_directories,
                                                          injection_file = injection_file_img1,
                                                          injection_waveform_arguments = injection_wf_args_img1,
                                                          analysis_waveform_arguments = analysis_wf_arg_img1)
    bp_args_img1, bp_unknown_args_img1, bp_parser_img1 = to_bilby_pipe.get_bilby_pipe_arguments(bp_config_img1)
    bp_input_img1 = bilby_pipe.main.MainInput(bp_args_img1, bp_unknown_args_img1)
    bp_input_img1.pretty_print_prior()
    bilby_pipe.main.write_complete_config_file(bp_parser_img1, bp_args_img1, bp_input_img1)

    # do the same for the second image
    joint_pe_golum_logger.info("Generating input info for image 2")
    bp_config_img2 = to_bilby_pipe.make_bilby_pipe_config(img2_config, args, output_directories,
                                                          injection_file = injection_file_img2,
                                                          injection_waveform_arguments = injection_wf_args_img2,
                                                          analysis_waveform_arguments = analysis_wf_arg_img2)
    bp_args_img2, bp_unknown_args_img2, bp_parser_img2 = to_bilby_pipe.get_bilby_pipe_arguments(bp_config_img2)
    bp_input_img2 = bilby_pipe.main.MainInput(bp_args_img2, bp_unknown_args_img2)
    bp_input_img2.pretty_print_prior()
    bilby_pipe.main.write_complete_config_file(bp_parser_img2, bp_args_img2, bp_input_img2)

    # also making the joint input. This is useful to get the correct labels 
    # for the runs.
    # dummy values are passed for the arguments as they won't be used
    bp_config_joint = to_bilby_pipe.make_bilby_pipe_config(img1_config, args, output_directories,
                                                          injection_file = injection_file_img1,
                                                          injection_waveform_arguments = injection_wf_args_img1,
                                                          analysis_waveform_arguments = analysis_wf_arg_img1)
    bp_args_joint, bp_unknown_args_joint, bp_parser_joint = to_bilby_pipe.get_bilby_pipe_arguments(bp_config_joint)
    bp_args_joint.label = bp_args_joint.label.split("image")[0]
    bp_input_joint = bilby_pipe.main.MainInput(bp_args_joint, bp_unknown_args_joint)
    bilby_pipe.main.write_complete_config_file(bp_parser_joint, bp_args_joint, bp_input_joint)
      
    # make the joint dag
    dag_generation.generate_joint_dag(bp_input_img1, bp_input_img2, bp_input_joint, args, config)

    joint_pe_golum_logger.info('Bilby pipe dag generation done')

    # make the final dag that submit them all
    if not args.local or config.getboolean('run_settings', 'local', fallback = False):
        joint_pe_golum_logger.info('Constructing the final dag')
        final_dag = utils.make_final_dag_joint(config, output_directories)
        if config.getboolean('condor_settings', 'submit', fallback = False):
            joint_pe_golum_logger.info(f"Submitting dag {final_dag} to condor")

            command = f'condor_submit_dag {final_dag}'
            submit_dag_proc = subprocess.Popen(shlex.split(command), stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = submit_dag_proc.communicate()
            joint_pe_golum_logger.info(out)
            
        else:
            joint_pe_golum_logger.info("To submit, use the following command; \n\t condor_submit_dag %s", final_dag)

if __name__ == "__main__":
    main()
