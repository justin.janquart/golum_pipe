"""
Script starting the routine to reweigh the joint samples
"""

import re
import sys
from golum_pipe.postprocessing.joint_samples_reweighting import main

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())