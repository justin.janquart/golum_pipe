"""
Script to use for the data generation of the second image
"""
import re
import sys
from golum_pipe.data_generation.image_2_data_generation import main 

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(main())