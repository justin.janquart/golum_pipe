#!/usr/bin/env python
"""
Script used to perform data analysis
for joint parameter estimation 
"""

import sys, os
import bilby_pipe
import bilby
import signal
import json
from golum_pipe.inference import joint_likelihood
from bilby_pipe.utils import nonestr, DataDump, CHECKPOINT_EXIT_CODE
from golum_pipe.core.utils import joint_pe_golum_logger

def create_joint_pe_analysis_parser():
    """
    creates a parser for joint parameter estimation
    """
    parser = bilby_pipe.parser.create_parser(top_level = False)

    # add the arguments related to joint PE to the parser
    parser.add('--detectors-image-1', action = 'append', 
              help = ("The name of detectors to use for the first image"))
    parser.add('--detectors-image-2', action = "append",
               help = ("The name of the detectors to use for the second image"))
    parser.add('--ini-file-img1', type = nonestr, default = None,
               help = ("ini file generated for the first image"))
    parser.add('--ini-file-img2', type = nonestr, default = None,
               help = ("ini file generated for the second image"))
    parser.add('--data-dump-file-img1', type = nonestr, default = None,
               help = ("Filename for the data dump for the first image"))
    parser.add('--data-dump-file-img2', type = nonestr, default = None,
               help = ("Filename for the data dump for the second image"))
    parser.add('--joint-prior-file', type = nonestr, default = None,
               help = ('Prior dictionary to be used for the run'))
    parser.add('--use-effective-parameters', type = bool, default = False,
               help = ('Whether to use the effective parameters for the run'))


    return parser

def sighandler(signum, frame):
    joint_pe_golum_logger.info("Performing periodic eviction")

    sys.exit(CHECKPOINT_EXIT_CODE)

class JointDataAnalysisInput(bilby_pipe.input.Input):
    """
    Class adapted to handle the joint parameter estimation framework
    """

    def __init__(self, args, unknown_args):
        """
        Input needs to be adapted to cope with the multi-signal case
        """

        super().__init__(args, unknown_args)

        # setup the output arguments
        self.result = None
        self.meta_data = {}
        self.outdir = args.outdir
        self.label = args.label
        self.result_format = args.result_format
        self.sampler = args.sampler
        self.scheduler = args.scheduler
        self.prior_file = args.joint_prior_file
        self.request_cpus = args.request_cpus 
        self.use_effective_parameters = args.use_effective_parameters
         
        
        # convert to an actual usable dictionary
        self.prior_dict = self.get_prior_from_file()
        self.meta_data['prior_file'] = self.prior_file
        joint_pe_golum_logger.info(f"prior dict {self.prior_dict}")
        # parse the ini and data dump files for the individual images
        self.ini_file_img1 = args.ini_file_img1
        self.ini_file_img2 = args.ini_file_img2
        self.data_dump_file_img1 = args.data_dump_file_img1
        self.data_dump_file_img2 = args.data_dump_file_img2

        # read data_dump file
        self.data_dump_img1 = self.read_data_dump(self.data_dump_file_img1, 1)
        self.data_dump_img2 = self.read_data_dump(self.data_dump_file_img2, 2)
        
        self.sampler_kwargs = args.sampler_kwargs

    def get_prior_from_file(self):
        """
        Function to set the prior from the stored file
        """

        # check if we can read the file
        if os.path.isfile(self.prior_file):
            pass
        elif os.path.isfile(os.path.basename(self.prior_file)):
            self.prior_file = os.path.basename(self.prior_file)
        else:
            raise ValueError(f"Unable to access the prior file {self.pror_file}")

        prior_dict = bilby.gw.prior.PriorDict().from_json(self.prior_file)

        return prior_dict

    def read_data_dump(self, file, img):
        """
        Function reading the data dump object
        for a given file

        ARGUMENTS:
        ---------
        - file: the data dump file from which the data should be read
        - img: the image for which the file is read

        RETURNS:
        --------
        - _data_dump: the data dump object produced from file
        """
        self.meta_data['data_dump_img%i'%img] = file

        joint_pe_golum_logger.info("Loading data dump")

        if os.path.isfile(file):
            pass
        elif os.path.isfile(os.path.basename(file)):
            file = os.path.basename(filename)
        else:
            raise FileNotFoundError(f"No dump file {file} found")

        _data_dump = DataDump.from_pickle(file)
        self.meta_data['image_%i_meta_data'%img] = {}
        self.meta_data['image_%i_meta_data'%img].update(_data_dump.meta_data)

        return _data_dump

    @property
    def result_directory(self):
        result_dir = os.path.join(self.outdir, "result")
        return os.path.relpath(result_dir)

    @property
    def sampling_seed(self):
        return self._sampling_seed

    @sampling_seed.setter
    def sampling_seed(self, sampling_seed):
        if sampling_seed is None:
            sampling_seed = np.random.randint(1, 1e6)
        self._sampling_seed = sampling_seed
        np.random.seed(sampling_seed)
        logger.info(f"Sampling seed set to {sampling_seed}")

        if not any(
            [
                k in self.sampler_kwargs
                for k in bilby.core.sampler.Sampler.sampling_seed_equiv_kwargs
            ]
        ):
            self.sampler_kwargs["sampling_seed"] = self._sampling_seed

    def get_likelihood_and_prior_single_image(self, ini_file, data_dump_file):
        """
        Function to fetch the prior and likelihood for a single 
        image
        """
        args, unknown_args = bilby_pipe.utils.parse_args([ini_file, '--data-dump-file', data_dump_file],
                                                         bilby_pipe.data_analysis.create_analysis_parser())
        single_image_analysis_input = bilby_pipe.data_analysis.DataAnalysisInput(args, unknown_args)

        return single_image_analysis_input.get_likelihood_and_priors()

    def get_likelihood_and_priors(self):
        """
        Function to fetch the likelihood and the priors.

        ARGUMENTS:
        ----------
        - Uses class attributes

        RETURNS:
        --------
        - likelihood, priors: the likelihood and the priors for which 
                              the analysis should be run 
        """

        priors = self.prior_dict.copy() 
        
        # for the likelihood, we need to pass the likelihood for each
        # event. So, need to get them from the data dump
        likeli_img1, prior_img1 = self.get_likelihood_and_prior_single_image(self.ini_file_img1, self.data_dump_file_img1)
        likeli_img2, prior_img2 = self.get_likelihood_and_prior_single_image(self.ini_file_img2, self.data_dump_file_img2)

        # update the priors to account for calibration
        for key in prior_img1.keys():
            if 'recalib_' in key:
                priors['%s_img1'%key] = prior_img1[key]
        for key in prior_img2.keys():
            if 'recalib_' in key:
                priors['%s_img2'%key] = prior_img2[key]

        if 'relative_magnification' in priors.keys():
            effective_parameters = False
        else:
            effective_parameters = True

        # setup the likelihood
        likelihood = joint_likelihood.JointGravitationalWaveTransient(likeli_img1, likeli_img2, 
                                                                      effective_parameters = effective_parameters)

        return likelihood, priors


    def run_sampler(self):
        """
        Function running the sampler
        """
        if self.scheduler.lower() == 'condor':
            signal.signal(signal.SIGALRM, handler = sighandler)

        likelihood, priors = self.get_likelihood_and_priors()

        self.result = bilby.run_sampler(likelihood = likelihood,
                                       priors = priors,
                                       sampler = self.sampler,
                                       label = self.label,
                                       outdir = self.result_directory,
                                       save = self.result_format,
                                       exit_code = CHECKPOINT_EXIT_CODE,
                                       **self.sampler_kwargs)




def main():
    """
    Setup the logic of the analysis
    """
    args, unknown_args = bilby_pipe.main.parse_args(sys.argv[1:], 
                                                     create_joint_pe_analysis_parser())

    joint_analysis = JointDataAnalysisInput(args, unknown_args)
    joint_analysis.run_sampler()
    sys.exit(0)
