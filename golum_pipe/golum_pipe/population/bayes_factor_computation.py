"""
Script actually doing the Bayes factor computation
"""

import os
import bilby 
import ast
import copy
import json
import signal
from importlib import import_module
import sys
import numpy as np
from configparser import ConfigParser
from bilby_pipe.main import parse_args
from bilby_pipe.bilbyargparser import BilbyArgParser
from golum_pipe.core.utils import symmetric_golum_logger, joint_pe_golum_logger, golum_logger_img2
from golum.tools import utils as golumutils
import golum
import scipy.special
from scipy.stats import gaussian_kde
from bilby_pipe.utils import nonestr, DataDump, CHECKPOINT_EXIT_CODE
from golum_pipe.data_analysis.joint_pe_data_analysis import sighandler

try:
    import hanabi.hierarchical as hierarchical
    import hanabi.inference as inference
    import hanabi.lensing as hanabi_lensing
except:
    print("Hanabi cannot be imported. Therefore, it cannot be used for selection effects")

try:
    import smeagol
except:
    print("Smeagol cannot be imported. Therefore, it cannot be used for selection effects")

try:
    import jax
    import jax.numpy as jnp
    from jax import jit, vmap
except:
    print("Jax cannot be imported. Therefore, it cannot be used for selection effects")


def create_populaiton_effects_parser():
    """
    Function creating the parser adapted for the population effects.
    In particular, it takes in the case we are in and the ini file
    """

    parser = BilbyArgParser(ignore_unknown_config_file_keys = True)
    parser.add('--ini-file', required = True, 
               help = 'The initial ini file used to start the run and from which the population information should be read')
    parser.add('--case', required = True, choices = ['symmetric', 'joint', 'golum'],
               help = 'The case in which we are working, should be chosen in [symmetric, joint]')

    return parser

def reweight_unlensed_evidence_and_posteriors(case, config, mass_model, spin_model, merger_rate, optical_depth, logger, image = 1):
    """
    Function reading in the evidence and the posteriors from the unlensed runs
    and reweighting them to account for the population model.

    ARGUMENTS:
    ---------
    - case: whether we are in the symmetric or joint case
    - config: the config as read from the ini file
    - mass_model: the initialized mass model
    - spin_model: the initialized model for the spins
    - merger_rate: the initialized merger rate model
    - optical_depth: the initialized optical depth model
    - logger: the logger to use to print out information
    - image: whether we want to reweight the image 1 or 2 posteriors.
             default is 1

    RETURNS:
    --------
    - population_reweighted_unlensed.reweight_ln_evidence(): the reweighed log evidence according to the population
    - population_reweighted_unlensed.reweight_samples(): the population reweighed under the unlensed hypothesis
    """

    unlensed_z_distribution = hierarchical.p_z.NotLensedSourceRedshiftProbDist(merger_rate_density = merger_rate,
                                                                               optical_depth = optical_depth)

    # fetch the result file to pass to the function 
    if case in ['symmetric', 'joint', 'golum']:
        unlensed_file = config.get('golum_settings', 'unlensed-file-image-%i'%image)
    else:
        raise ValueError(f"Unrecognized case {case}")
    
    
    # apply the reweighting as to be compatible with Hanabi
    unlensed_result = bilby.result.read_in_result(filename = unlensed_file)

    # check if the component masses have been generated. If not, do the reweighting
    if 'mass_1' not in unlensed_result.posterior.keys():
        logger.info("Component masses not in posteriors, generating them")
        unlensed_result.posterior['mass_1'], unlensed_result.posterior['mass_2'] = bilby.gw.conversion.chirp_mass_and_mass_ratio_to_component_masses(
                                                                                     unlensed_result.posterior['chirp_mass'],
                                                                                     unlensed_result.posterior['mass_ratio'])

    # verify the priors for which the run happened and if needed convert it
    if 'mass_1' in unlensed_result.priors.keys():
        if isinstance(unlensed_result.priors['mass_1'], bilby.core.prior.Constraint):
            logger.info("Converting the mass priors")
            # need to convert to the correct priors
            priors, samples, log_evidence = smeagol.golum_to_hanabi.convert_Mc_q_prior_to_component_masses_priors(unlensed_result.priors.copy(),
                                                                                                                  unlensed_result.log_evidence,
                                                                                                                  unlensed_result.posterior.copy(),
                                                                                                                  fraction = 1)
        elif isinstance(unlensed_result.priors['mass_1'], bilby.core.prior.Uniform):
            # no need to convert anything. Just changing the naming to avoid issues
            priors = unlensed_result.priors.copy()
            samples = unlensed_result.posterior.copy()
            log_evidence = unlensed_result.log_evidence
    else:
        if isinstance(unlensed_result.priors['chirp_mass'], bilby.core.prior.Uniform):
            logger.info('No prior found on component masses. Converting to a constraint one')
            logger.warning('This is an assumption')
            unlensed_result.priors['mass_1'] = bilby.core.prior.Uniform(name = 'mass_1', minimum = 1., maximum = 1000.,
                                                                        latex_label = r'$M_1$')
            unlensed_result.priors['mass_2'] = bilby.core.prior.Uniform(name = 'mass_2', minimum = 1., maximum = 1000.,
                                                                        latex_label = r'$M_2$')
            # convert the priors
            priors, samples, log_evidence = smeagol.golum_to_hanabi.convert_Mc_q_prior_to_component_masses_priors(unlensed_result.priors.copy(),
                                                                                                                  unlensed_result.log_evidence,
                                                                                                                  unlensed_result.posterior.copy(),
                                                                                                                  fraction = 1)

    # do the actual reweighting
    population_reweighted_unlensed = smeagol.reweight_with_population_model.ReweightWithPopulationModel(samples,
                                                                                                             log_evidence,
                                                                                                             priors,
                                                                                                             mass_model,
                                                                                                             spin_model,
                                                                                                             unlensed_z_distribution)


    return population_reweighted_unlensed.reweight_ln_evidence(), population_reweighted_unlensed.reweight_samples()

def get_joint_information_for_symmetric(config):
    """
    Function to get the joint information for a symmetric run. 
    In this case, information is a bit scattered and we need to 
    find the information and make it as one

    ARGUMENTS:
    ----------
    - config: the config read from the ini file and containing the 
              information required to reconstruct the information

    RETURNS:
    --------
    - joint_samples: a dictionary joining the samples for the two
                     images
    - joint_priors: a single prior dictionary regrouping the prior
                    for the two images
    - joint_log_evidence: the joint evidence regrouping the
                          information from the runs
    """

    # read in the joint samples
    outdir = config.get('output_settings', 'outdir')
    label = config.get('output_settings', 'label')
    result_dir = f'{outdir}/final_result'
    joint_sample_file = f'{result_dir}/{label}_joint_reweighted_samples.json'
    if os.path.isfile(joint_sample_file):
        with open(joint_sample_file, 'r') as f_rew:
            joint_samples = json.load(f_rew)
    else:
        raise ValueError(f"Reweighted samples file {joint_sample_file} not found")
    # convert joint samples to array to make the rest easier
    for key in joint_samples.keys():
        joint_samples[key] = np.array(joint_samples[key])
    # make a joint prior file 
    # grab the results for the lensed img1 run 
    img1_lensed_file = config.get('golum_settings', 'posterior-file-image-1')
    img1_results = bilby.result.read_in_result(filename = img1_lensed_file)
    img1_prior = copy.copy(img1_results.priors)
    img1_lens_log_evidence = img1_results.log_evidence

    # still need to adapt the samples to the hanabi conventions
    # add mass samples if needed 
    if 'mass_1' not in joint_samples:
        joint_samples['mass_1'], joint_samples['mass_2'] = bilby.gw.conversion.chirp_mass_and_mass_ratio_to_component_masses(joint_samples['chirp_mass'],
                                                                                                                             joint_samples['mass_ratio'])

    img1_prior, joint_samples, img1_lens_log_evidence = smeagol.golum_to_hanabi.convert_Mc_q_prior_to_component_masses_priors(img1_prior,
                                                                                                                             img1_lens_log_evidence,
                                                                                                                             joint_samples, fraction = 1)

    # fetch the results for the golum run 
    result_file_img1img2 = None
    for file in os.listdir(result_dir):
        if 'img1img2' in file and '_result' in file:
            result_file_img1img2 = f'{result_dir}/{file}'
    golum_result_img1img2 = bilby.result.read_in_result(filename = result_file_img1img2)
    golum_prior = copy.copy(golum_result_img1img2.priors)
    img1img2_log_evidence = golum_result_img1img2.log_evidence

    # need to make adapted joint priors for hanabi
    if 'relative_magnification' not in golum_prior.keys():
        params_to_check = []
        for i in range(1, 5):
            params_to_check.append('luminosity_distance_%i'%i)
            params_to_check.append('geocent_time_%i'%i)
            params_to_check.append('n_phase_%i'%i)
    else:
        params_to_check = ['luminosity_distance', 'geocent_time', 'n_phase']

    joint_priors = copy.copy(img1_prior)
    for key in list(joint_priors.keys()):
        if '_1' in key and key in params_to_check:
            t = key.split('_')
            base_key = t[0] + '_' + t[1]
            joint_priors['%s^(1)'%base_key] = copy.copy(joint_priors[key])
            joint_priors.pop(key)
        elif key in params_to_check:
            joint_priors['%s^(1)'%key] = copy.copy(joint_priors[key])
            joint_priors.pop(key)

    for key in golum_prior.keys():
        if key not in params_to_check:
            joint_priors[key] = copy.copy(golum_prior[key])
        else:
            t = key.split('_')
            base_key = t[0] + '_' + t[1]
            joint_priors['%s^(%s)'%(base_key, t[2])] = copy.copy(joint_priors[key])

    # make the joint evidence 
    img2_lensed_file = config.get('golum_settings', 'posterior-file-image-2')
    
    # also need the last result file
    result_file_img2img1 = None
    for file in os.listdir(result_dir):
        if 'img2img1' in file and '_result' in file:
            result_file_img2img1 = f"{result_dir}/{file}"
    golum_result_img2img1 = bilby.result.read_in_result(filename = result_file_img2img1)
    img2img1_log_evidence = golum_result_img2img1.log_evidence

    # check the missing evidence 
    img2_lensed_result = bilby.result.read_in_result(filename = img2_lensed_file)
    img2_lens_log_evidence = img2_lensed_result.log_evidence

    # also need to adapt the other evidence for the change in samples
    img2_prior, joint_samples_2, img2_lens_log_evidence = smeagol.golum_to_hanabi.convert_Mc_q_prior_to_component_masses_priors(img1_prior,
                                                                                                                             img1_lens_log_evidence,
                                                                                                                             joint_samples, fraction = 1)

    

    part_1 = img1img2_log_evidence + img1_lens_log_evidence 
    part_2 = img2img1_log_evidence + img2_lens_log_evidence 

    joint_log_evidence = scipy.special.logsumexp([part_1, part_2])

    # convert the key for the joint samples
    if 'relative_magnification' in list(joint_samples.keys()) and 'luminosity_distance' in list(joint_samples.keys()):
        joint_samples['luminosity_distance^(1)'] = joint_samples['luminosity_distance']
        joint_samples['geocent_time^(1)'] = joint_samples['geocent_time']
        joint_samples['n_phase^(1)'] = joint_samples['n_phase']
    else:
        for key in list(joint_samples.keys()):
            if 'luminosity_distance' in key or 'geocent_time' in key or 'n_phase' in key:
                t = key.split('_')
                base_key = t[0] + '_' + t[1]
                joint_samples['%s^(1)'%base_key] = joint_samples[key]


    return joint_samples, joint_priors, joint_log_evidence

def get_joint_information_for_golum(config):
    """
    Get the joint information from a golum run 

    ARGUMENTS:
    ----------
    - config: the configuration obtained from reading the 
              ini file

    RETURNS:
    --------
    - joint_samples: a dictionary joining the samples for the two
                     images
    - joint_priors: a single prior dictionary regrouping the prior
                    for the two images
    - joint_log_evidence: the joint evidence regrouping the
                          information from the runs
    """

    # read in the samples
    outdir = config.get('output_settings', 'outdir')
    label = config.get('output_settings', 'label')
    result_dir = f'{outdir}/final_result'
    joint_sample_file = None
    for file in os.listdir(result_dir):
        if '_reweighted_samples' in file:
            joint_sample_file = f'{result_dir}/{file}'
    if joint_sample_file is not None:
        ref_img_samples, golum_samples = golumutils.read_reweighted_posterior(file = joint_sample_file)
    else:
        raise ValueError("Reweighted samples file not found")

    # also need the result file for the second and first image 
    img1_file = config.get("golum_settings", "posterior-file-image-1")
    img1_results = bilby.result.read_in_result(filename= img1_file)
    img1_priors = copy.copy(img1_results.priors)
    img1_log_evidence = img1_results.log_evidence

    img2_resfile = None
    for file in os.listdir(result_dir):
        if "_result" in file:
            img2_resfile = f'{result_dir}/{file}'

    if img2_resfile is None:
        raise ValueError("Result file for img2 not found")
    
    img2_result = bilby.result.read_in_result(filename = img2_resfile)
    img2_log_evidence = img2_result.log_evidence
    img2_priors = copy.copy(img2_result.priors)

    # make joint samples and check the naming
    samples = {}
    for key in ref_img_samples.keys():
        if key not in ['luminosity_distance', 'geocent_time', 'n_phase']:
            samples[key] = np.array(ref_img_samples[key])
        else:
            samples['%s^(1)'%key] = np.array(ref_img_samples[key])
    for key in golum_samples.keys():
        if key not in ['luminosity_distance', 'geocent_time', 'n_phase']:
            samples[key] = np.array(golum_samples[key])
        else:
            samples['%s^(2)'%key] = np.array(golum_samples[key])

    # check if we need to generate the masses
    if 'mass_1' not in samples.keys():
        samples['mass_1'], samples['mass_2'] = bilby.gw.conversion.chirp_mass_and_mass_ratio_to_component_masses(samples['chirp_mass'],
                                                                                                                 samples['mass_ratio'])
    # convert the info for the first image
    priors, joint_samples, evidence = smeagol.golum_to_hanabi.convert_Mc_q_prior_to_component_masses_priors(img1_priors,
                                                                                                      img1_log_evidence,
                                                                                                      samples, fraction = 1)

    # make the joint priors
    joint_priors = {}
    for key in priors.keys():
        if key not in ['geocent_time', 'luminosity_distance', 'n_phase']:
            joint_priors[key] = priors[key]
        else:
            joint_priors['%s^(1)'%key] = priors[key]
    for key in img2_priors.keys():
        if key not in ['geocent_time', 'luminosity_distance', 'n_phase']:
            joint_priors[key] = img2_priors[key]
        else:
            joint_priors['%s^(2)'%key] = img2_priors[key]

    joint_log_evidence = evidence + img2_log_evidence

    return joint_samples, joint_priors, joint_log_evidence


def get_joint_information_for_joint(config):
    """
    Get the symmetric information for the joint run

    ARGUMENTS:
    ----------
    - config: the configuration obtained from reading the ini file

    RETURNS:
    --------
    - joint_samples: a dictionary joining the samples for the two
                     images
    - joint_priors: a single prior dictionary regrouping the prior
                    for the two images
    - joint_log_evidence: the joint evidence regrouping the
                          information from the runs
    """

    # read in the result file
    outdir = config.get('output_settings', 'outdir')
    label = config.get('output_settings', 'label')
    result_dir = f'{outdir}/final_result'
    result_file = None
    for file in os.listdir(result_dir):
        if '_result' in file:
            result_file = f"{result_dir}/{file}"
    if file is None:
        raise ValueError("Result file not found")

    # read in the results
    joint_result = bilby.result.read_in_result(filename = result_file)

    # need to adapt the output to hanabi standards
    joint_samples = joint_result.posterior.copy()
    if 'mass_1' not in joint_samples.keys():
        joint_samples['mass_1'], joint_samples['mass_2'] = bilby.gw.conversion.chirp_mass_and_mass_ratio_to_component_masses(joint_samples['chirp_mass'],
                                                                                                                             joint_samples['mass_ratio'])
    if 'relative_magnification' in joint_samples.keys():
        joint_samples['luminosity_distance^(1)'] = joint_samples['luminosity_distance']
        joint_samples['geocent_time^(1)'] = joint_samples['geocent_time']
        joint_samples['n_phase^(1)'] = joint_samples['n_phase']
    else:
        joint_samples['luminosity_distance^(1)'] = joint_samples['luminosity_distance_1']
        joint_samples['geocent_time^(1)'] = joint_samples['geocent_time_1']
        joint_samples['n_phase^(1)'] = joint_samples['n_phase_1']
        joint_samples['luminosity_distance^(2)'] = joint_samples['luminosity_distance_2']
        joint_samples['geocent_time^(2)'] = joint_samples['geocent_time_2']
        joint_samples['n_phase^(2)'] = joint_samples['n_phase_2']

    joint_priors, joint_samples, joint_log_evidence = smeagol.golum_to_hanabi.convert_Mc_q_prior_to_component_masses_priors(joint_result.priors.copy(),
                                                                                                                            joint_result.log_evidence,
                                                                                                                            joint_samples,
                                                                                                                            fraction = 1)

    # adapt the priors to hanabi conventions
    if 'relative_magnification' not in joint_result.priors.keys():
        params_to_check = []
        for i in range(1, 5):
            params_to_check.append('luminosity_distance_%i'%i)
            params_to_check.append('geocent_time_%i'%i)
            params_to_check.append('n_phase_%i'%i)
    else:
        params_to_check = ['luminosity_distance', 'geocent_time', 'n_phase']

    joint_priors = {}
    for key in joint_result.priors.keys():
        if key not in params_to_check:
            joint_priors[key] = joint_result.priors[key]
        else:
            if 'relative_magnification' not in joint_result.priors.keys():
                if '_1' in key:
                    t = key.split('_')
                    base_key = t[0] + '_' + t[1]
                    joint_priors['%s^(1)'%(base_key)] = joint_result.priors[key]
                else:
                    t = key.split('_')
                    base_key = t[0] + '_' + t[1]
                    joint_priors['%s^(%s)'%(base_key, t[2])] = joint_result.priors[key]
            else:
                joint_priors['%s^(1)'%key] = joint_result.priors[key]


    return joint_samples, joint_priors, joint_log_evidence

def get_joint_information_for_lensed_population_effects(case, config):
    """
    Function to fetch the information required to set up the lensed
    population reweighting. This has to be adapted depending on 
    the considered case. 

    ARGUMENTS:
    ----------
    - case: the case in which we are (symmetric, joint, ...)
    - config: the configuration gotten from the ini. This should
              be enough to reconstruct the information we want

    RETURNS:
    --------
    - joint_samples: a dictionary joining the samples for the two
                     images
    - joint_priors: a single prior dictionary regrouping the prior
                    for the two images
    - joint_log_evidence: the joint evidence regrouping the
                          information from the runs
    """

    if case == 'symmetric':
        joint_samples, joint_priors, joint_log_evidence = get_joint_information_for_symmetric(config)
    elif case == 'joint':
        joint_samples, joint_priors, joint_log_evidence = get_joint_information_for_joint(config)
    elif case == 'golum':
        joint_samples, joint_priors, joint_log_evidence = get_joint_information_for_golum(config)
    else:
        raise ValueError(f"Unrecognized case {case}")

    return joint_samples, joint_priors, joint_log_evidence

class PopulationReweightingInput(object):
    """
    Class to handle the population reweighting likelihood
    and run.
    """

    def __init__(self, args, config, joint_samples, joint_priors, joint_log_evidence, mass_model, spin_model, magnification_distribution, merger_rate_model, optical_depth, n_samples, kde, use_jax):
        """
        Initialization of the class. 
        The input here is fully different 

        ARGUMENTS:
        ---------
        - args: the arguments obtained when parsing information
        - config: the configuration information obtained by reading
                  the ini file
        - joint_samples: the joint samples used to compute the information
        - joint_priors: the joint priors used to compute the initial evidence
        - joint_log_evidence: the joint lensed evidence before population inclusion 
        - mass_model: the model used to describe the masses
        - spin_model: the model used to describe the spins
        - magnification_distribution: either the analytical distribution used for magnification 
                                      or the object (gaussian_kde, trained NF model, DPGMM) used
                                      to construct the magnification distribution
        - merger_rate_mode:the model used to describe the merger rate density
        - optical_depth: the optical depth to use 
        - n_samples: the number of samples to consider when going from (Dl1, mu_rel) to (Dl1, Dl2) parametrization
                     or the number of samples to use when already having the correct parametrization
        - kde: bool, whether we use a kde model or not. If false, we assume that we use an analytical SIS model
        - use_jax: bool, whether we should use jax to do the kde or not. 
        """

        # setup the output arguments
        self.result = None
        self.meta_data = {}
        self.outdir = f'{config.get("output_settings", "outdir")}/final_result'
        self.label = config.get('output_settings', "label")
        self.result_format = config.get('output_settings', 'output-format', fallback = 'hdf5')
        self.sampler = config.get('population_settings', 'pop-sampler', fallback = 'dynesty')
        self.sampler_kwargs = config.getdict('population_settings', 'pop-sampler-kwargs', fallback = {"n_points" : 2048, "npool" : 16, "nact" : 5})
        self.scheduler = config.get('condor_settings', 'scheduler', fallback = 'condor')
        self.request_cpus = config.getint('population_settings', 'pop-request-cpus', fallback = 16)
        if self.request_cpus > 1:
            # make sure we use all the requested cpus
            self.sampler_kwargs['npool'] = self.request_cpus

        # useful things for the likelihood
        self.joint_samples = joint_samples
        self.joint_priors = joint_priors
        self.joint_log_evidence = joint_log_evidence
        self.mass_model = mass_model
        self.spin_model = spin_model
        self.magnification_distribution = magnification_distribution
        self.n_samples = n_samples
        self.kde = kde
        self.use_jax = use_jax

        # need to set up the prior 
        self.lensed_z_distribution = hierarchical.p_z.LensedSourceRedshiftProbDist(merger_rate_density = merger_rate_model,
                                                                                   optical_depth = optical_depth)
        self.prior_dict = {'redshift' : self.lensed_z_distribution}

    def get_likelihood_and_priors(self):
        """
        Function to set up the likelihood and priors

        ARGUMENTS:
        ----------
        - Uses class attributes

        RETURNS:
        --------
        - likelihood, priors: the likelihood and the priors for which 
                              the analysis should be run 
        """

        priors = copy.copy(self.prior_dict)

        # setup the likelihood
        likelihood = smeagol.marginalized_likelihood.MonteCarloMarginalizedLikelihood(self.joint_samples, self.joint_log_evidence,
                                                                                      self.mass_model, self.spin_model,
                                                                                      self.magnification_distribution,
                                                                                      sampling_priors = self.joint_priors,
                                                                                      n_samples = self.n_samples,
                                                                                      kde = self.kde, use_jax = self.use_jax)

        return likelihood, priors


    def run_sampler(self):
        """
        Function running the sampler
        """

        if self.scheduler.lower() == 'condor':
            signal.signal(signal.SIGALRM, handler = sighandler)

        likelihood, priors = self.get_likelihood_and_priors()

        self.result = bilby.run_sampler(likelihood = likelihood,
                                        priors = priors,
                                        sampler = self.sampler,
                                        label = self.label,
                                        outdir = self.outdir,
                                        save = self.result_format,
                                        exit_code = CHECKPOINT_EXIT_CODE,
                                        **self.sampler_kwargs)

def main():
    """
    Starts the Bayes factor computation
    """

    # parse the ini file
    args, unknown_args = parse_args(sys.argv[1:], create_populaiton_effects_parser())

    # setup the model functions for all the run 
    if args.case == 'symmetric':
        logger = symmetric_golum_logger
    elif args.case == 'joint':
        logger = joint_pe_golum_logger
    elif args.case == 'golum':
        logger = golum_logger_img2

    # read in the ini file
    config = ConfigParser(converters = {'dict' : lambda x: ast.literal_eval(x)})

    # check that the ini file exist and read it
    if not os.path.isfile(args.ini_file):
        raise IOError(f'{args.ini_file} does not exist')

    try:
        config.read(args.ini_file)
    except IOError:
        raise ValueError(f'Cannot read file {args.ini_file}')

    # setup the mass model
    mass_model_str = config.get('population_settings', 'mass-model')
    mass_model_parameters = config.getdict('population_settings', 'mass-model-parameters')
    logger.info(f'Selection effects computed with mass model {mass_model_str}')
    logger.info(f'Mass model parameters: {mass_model_parameters}')
    mass_model_fct = getattr(hierarchical.source_population_model, mass_model_str)
    mass_model = mass_model_fct(**mass_model_parameters)

    # setup the spin model
    spin_model_str = config.get('population_settings', 'spin-model', fallback = 'None')
    spin_model_parameters = config.getdict('population_settings', 'spin-model-parameters', fallback = 'None')
    logger.info(f'Selection effects computed with spin model {spin_model_str}')
    logger.info(f'Spin model parameters: {spin_model_parameters}')
    if spin_model_str != 'None':
        spin_model_fct = getattr(hierarchical.source_population_model, spin_model_str)
    else:
        spin_model_fct = None
    if spin_model_parameters != 'None':
        spin_model = spin_model_fct(**spin_model_parameters)
    else:
        if spin_model_fct is not None:
            spin_model = spin_model_fct()
        else:
            spin_model = None

    # setup the merger rate model
    merger_rate_model_str = config.get('population_settings', 'merger-rate-model')
    merger_rate_parameters = config.getdict('population_settings', 'merger-rate-parameters')
    logger.info(f'Selection effects computed with merger rate density model {merger_rate_model_str}')
    logger.info(f'Merger rater parameters: {merger_rate_parameters}')
    merger_rate_fct = getattr(hierarchical.merger_rate_density, merger_rate_model_str)
    merger_rate_model = merger_rate_fct(**merger_rate_parameters)

    # setup the optical depth
    optical_depth_model_str = config.get('population_settings', 'optical-depth')
    logger.info(f'Selection effects computed with optical depth {optical_depth_model_str}')
    optical_depth_model_fct = getattr(hanabi_lensing.optical_depth, optical_depth_model_str)
    optical_depth_model = optical_depth_model_fct()

    # do the unlensed reweighting
    logger.info("Performing unlensed population effects for the first image")
    reweighted_log_Z_img1, reweighted_posteriors_img1 = reweight_unlensed_evidence_and_posteriors(args.case, config,
                                                                                                  mass_model, spin_model,
                                                                                                  merger_rate_model, optical_depth_model,
                                                                                                  logger,
                                                                                                  image = 1)

    logger.info("Performing unlensed population effects for the second image")
    reweighted_log_Z_img2, reweighted_posteriors_img2 = reweight_unlensed_evidence_and_posteriors(args.case, config,
                                                                                                  mass_model, spin_model,
                                                                                                  merger_rate_model, optical_depth_model, logger,
                                                                                                  image = 2)

    # do the lensed reweighting 
    magnification_distribution = config.get('population_settings', 'magnification-model', fallback = 'None')
    if magnification_distribution == 'None':
        magnification_model_file = config.get('population_settings', 'magnification-model-file')
        logger.info(f"Lensed population effects done with file {magnification_model_file}")
    else:
        logger.info(f"Lensed population effect done with lensing model {magnification_distribution}")
    
    joint_samples, joint_priors, joint_log_evidence = get_joint_information_for_lensed_population_effects(args.case, config)
    
    n_samples = config.get('population_settings', 'n-samples', fallback = 'None')
    if n_samples == 'None':
        n_samples = None
    else:
        n_samples = int(n_samples)

    # prepare correctly the data needed for the analysis
    use_kde = config.getboolean('population_settings', 'use-kde', fallback = False)
    if use_kde:
        use_jax = config.getboolean('population_settings', 'use-jax', fallback = False)
        if use_jax:
            logger.info("Using JAX to perform the selection effects")
            with open(magnification_model_file, 'r') as fcat:
                data = json.load(fcat)

            magnification_distribution = smeagol.lensing_models.make_jax_kde(data)
        else:
            logger.info("Using gaussian KDE to make the model from file")
            with open(magnification_model_file, 'r') as fcat:
                data = json.load(fcat)

            vals_kde = np.vstack([data['mu1'], data['mu2']])
            magnification_distribution = gaussian_kde(vals_kde)
    else:
        logger.info("Doing the population reweighting based on SIS analytical model")
        use_jax = False



    population_reweighting = PopulationReweightingInput(args, config, joint_samples, joint_priors, joint_log_evidence, 
                                                        mass_model, spin_model, magnification_distribution, merger_rate_model,
                                                        optical_depth_model, n_samples, use_kde, use_jax)
    population_reweighting.run_sampler()

    # once the run is done, combine all the results together to make
    # the actual final value
    lensed_log_evidence_pop = population_reweighting.result.log_evidence

    log_prior_odds = config.getfloat('population_settings', 'log-prior-odds', fallback = 0)
    
    if log_prior_odds == 0:
        logger.info('No odds ratio specified, focusing on bayes factor')
        log_unlensed_select_factor = config.getfloat('population_settings', 'log-unlensed-selection-factor', fallback = 1)
        log_lensed_select_factor = config.getfloat('population_settings', 'log-lensed-selection-factor', fallback = 1)
        log_clu_pop = lensed_log_evidence_pop - reweighted_log_Z_img1 - reweighted_log_Z_img2
        log_odds_ratio = None
        if log_unlensed_select_factor == 1 and log_lensed_select_factor == 1:
            logger.info('No selection factor specified. We will get an unnormalized Bayes factor')
            log_blu = None
        else:
            logger.info('Computing normalized bayes factor')
            log_blu = (2*log_unlensed_select_factor)-log_lensed_select_factor + log_clu_pop
    
    else:
        logger.info("Computing the odds ratio using prior odds")
        log_clu_pop = lensed_log_evidence_pop - reweighted_log_Z_img1 - reweighted_log_Z_img2
        log_odds_ratio = log_clu_pop + log_prior_odds
    
    # save the final result in an output file
    outdir = config.get('output_settings', 'outdir')
    label = config.get('output_settings', 'label')
    with open(f'{outdir}/final_result/{label}_result_population_effects.json', 'w') as fout:
        json.dump({'log_clu_pop' : log_clu_pop, 'log_blu' : log_blu,
                   'log_odds_ratio' : log_odds_ratio}, fout,
                   indent = 4)

    sys.exit(0)
