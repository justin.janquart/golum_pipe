"""
Script taking care of the Symmetric runs.
It reads the .ini file and set the data generation and analysis up
for a symmetric run
"""

import argparse
import bilby 
import bilby_pipe
import shlex
import subprocess

from golum_pipe.core import utils, golum_parsers, to_bilby_pipe
from golum_pipe.core.utils import symmetric_golum_logger
from golum_pipe.input_handling import main_input
from golum_pipe.job_generation import dag_generation 

def main():
    """
    Main function to do the analysis. This should read in the 
    arguments and start the different jobs as child jobs. We
    make the individual dag files for the the two GOLUM runs
    and then start them by making a dag calling them .
    """

    # start by reading the arguments
    golumparser = golum_parsers.set_golum_parser()
    args = golumparser.parse_args()

    # read in the ini and check for sanity
    config = utils.verify_config_file(args)

    # verifiy if it is an injection and whether the run 
    # should be local
    args.injection_img1 = config.getboolean('injection_settings_image_1', 'injection', fallback=False)

    args.injection_img2 = config.getboolean('injection_settings_image_2', 'injection', fallback=False)

    if config.getboolean('condor_settings', 'local'):
        args.local = True

    # setting the output directories
    outdir = config.get('output_settings' ,'outdir', fallback = ".")
    output_directories = {'outdir' : outdir, 'data' : f'{outdir}/data',
                          'submit' : f'{outdir}/submit'}
    label = config.get('output_settings', 'label', fallback = 'dummy_label')

    for _, directory in output_directories.items():
        bilby.core.utils.check_directory_exists_and_if_not_mkdir(directory)


    # setup the logger
    logging_level = config.get('output_settings', 'logging-level', fallback = 'INFO')
    utils.setup_logger(output_directories['outdir'], logging_level, args = args)
    symmetric_golum_logger.info(f'Golum logger is setup, log output in {outdir}/golum_symmetric.log')

    # need to make the ini file for the first and second image
    symmetric_golum_logger.info(f'Writing ini file for the image 1 - image 2 GOLUM run')
    img1img2_config = utils.make_golum_config_from_symmetric(config, n_img = 1)
    with open(f'{outdir}/{label}_img1img2_golum.ini', 'w') as img1img2_ini:
        img1img2_config.write(img1img2_ini)
    img1img2_inifile = f'{outdir}/{label}_img1img2_golum.ini'


    # do the same for the second image
    symmetric_golum_logger.info(f'Writing ini file for the image 2 - image 1 GOLUM run')
    img2img1_config = utils.make_golum_config_from_symmetric(config, n_img = 2)
    with open(f'{outdir}/{label}_img2img1_golum.ini', 'w') as img2img1_ini:
        img2img1_config.write(img2img1_ini)

    img2img1_inifile = f'{outdir}/{label}_img2img1_golum.ini'

    # make the dag for the img1 - img2 run 
    # verify if injection, and if it is, make the injection file
    if args.injection_img2 or config.getboolean('injection_settings_image_2', 'injection'):
        symmetric_golum_logger.info('Generating injection information for the second image')
        injection_wf_args_img2 = utils.make_waveform_arguments(img1img2_config, injection = True)
        symmetric_golum_logger.info('Injection waveform arguments img 2: %s', injection_wf_args_img2)
        injection_file_img2 = utils.make_injection_file(img1img2_config, img2 = True, symmetric = 'img1img2')
        symmetric_golum_logger.info('Injection file for img1 - img2 analysis generated')
    else:
        injection_file_img2 = None
        injection_wf_args_img2 = None

    symmetric_golum_logger.info("Generating analysis waveform arguments for img1 - img2")
    analysis_waveform_arguments_img1img2 = utils.make_waveform_arguments(img1img2_config)
    symmetric_golum_logger.info("Analysis waveform arguments: %s", analysis_waveform_arguments_img1img2)

    args.injection = args.injection_img2
    bp_config_img1img2 = to_bilby_pipe.symmetric_args_to_bilby_pipe_config(img1img2_config,
                                               args, output_directories, 2,
                                               injection_file = injection_file_img2,
                                               injection_waveform_arguments = injection_wf_args_img2,
                                               analysis_waveform_arguments = analysis_waveform_arguments_img1img2)
    
    bp_args_img1img2, bp_unknown_args_img1img2, bp_parser_img1img2 = to_bilby_pipe.get_bilby_pipe_arguments(bp_config_img1img2)
    bp_input_img1img2 = main_input.GolumImage2MainInput(bp_args_img1img2, bp_unknown_args_img1img2)
    bp_input_img1img2.pretty_print_prior()
    bilby_pipe.main.write_complete_config_file(bp_parser_img1img2, bp_args_img1img2, bp_input_img1img2)
    dag_generation.generate_dag_second_image(bp_input_img1img2, None, None)
    dag_img1img2 = f'{output_directories["submit"]}/dag_{bp_input_img1img2.label}.submit' 

    symmetric_golum_logger.info(f'Generated img1 - img2 dag: {dag_img1img2}')

    # do the same for the second image
    if args.injection_img1 or config.getboolean('injection_settings_image_1', 'injection'):
        symmetric_golum_logger.info('Generating injection information for the second image')
        injection_wf_args_img1 = utils.make_waveform_arguments(img2img1_config, injection = True)
        symmetric_golum_logger.info('Injection waveform arguments img 1: %s', injection_wf_args_img1)
        injection_file_img1 = utils.make_injection_file(img2img1_config, img2 = True, symmetric = 'img2img1')
        symmetric_golum_logger.info('Injection file for img2 - img1 analysis generated')
    else:
        injection_file_img1 = None
        injection_wf_args_img1 = None

    symmetric_golum_logger.info("Generating analysis waveform arguments for img2 - img1")
    analysis_waveform_arguments_img2img1 = utils.make_waveform_arguments(img2img1_config)
    symmetric_golum_logger.info("Analysis waveform arguments: %s", analysis_waveform_arguments_img2img1)


    args.injection = args.injection_img1
    bp_config_img2img1 = to_bilby_pipe.symmetric_args_to_bilby_pipe_config(img2img1_config,
                                               args, output_directories, 1,
                                               injection_file = injection_file_img1,
                                               injection_waveform_arguments = injection_wf_args_img1,
                                               analysis_waveform_arguments = analysis_waveform_arguments_img2img1)
    bp_args_img2img1, bp_unknown_args_img2img1, bp_parser_img2img1 = to_bilby_pipe.get_bilby_pipe_arguments(bp_config_img2img1)
    bp_input_img2img1 = main_input.GolumImage2MainInput(bp_args_img2img1, bp_unknown_args_img2img1)
    bp_input_img2img1.pretty_print_prior()
    bilby_pipe.main.write_complete_config_file(bp_parser_img2img1, bp_args_img2img1, bp_input_img2img1)
    dag_generation.generate_dag_second_image(bp_input_img2img1, None, None)
    dag_img2img1 = f'{output_directories["submit"]}/dag_{bp_input_img2img1.label}.submit' 

    symmetric_golum_logger.info(f'Generated img2 - img1 dag: {dag_img2img1}')

    # generate (and possibly submit) the final submission file calling the img1img2, img2img1 and post-processing
    scheduler = config.get('condor_settings', 'scheduler', fallback='condor')
    submit = config.getboolean('condor_settings', 'submit', fallback = False)
    if scheduler.lower() in ['condor', 'slurm']:
        # this is also needed in the slurm case, since bilby_pipe autogenerates the img1img2/img2img1 slurm files via the DAG
        final_dag = dag_generation.generate_dag_symmetric(args, config, output_directories, dag_img1img2, dag_img2img1)
    if scheduler.lower() == 'condor':
        if submit:
            symmetric_golum_logger.info(f'Submitting dag {final_dag} to condor')
            command = f'condor_submit_dag {final_dag}'
            submit_dag_proc = subprocess.Popen(shlex.split(command), stdout = subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = submit_dag_proc.communicate()
            symmetric_golum_logger.info(f'{out}')
        else:
            symmetric_golum_logger.info('To submit, use the following command: \n\t$ condor_submit_dag %s', final_dag)
    elif scheduler.lower() == 'slurm':
        from . import slurm
        slurm_master_file = slurm.build_slurm_submit_symmetric(args, config, output_directories)
        # print out how to submit
        command_line = f'bash {slurm_master_file}'
        if submit:
            symmetric_golum_logger.info(f'Submitting master submit file {slurm_master_file} to slurm')
            subprocess.run([command_line], shell=True)
        else:
            symmetric_golum_logger.info(f'Overall master slurm script written, to run jobs submit:\n$ {command_line}')

if __name__ == "__main__":
    main()
