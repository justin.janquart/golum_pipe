"""
Script with a joint likelihood adapted to the 
pipe format
"""

import bilby
import numpy as np
import attr
import copy

from bilby.core.likelihood import Likelihood
from collections import namedtuple


def make_strain_from_relative_lensing_parameters(img_likelihood, parameters):
    """
    Function converting the strain stored in one likelihood to the 
    strain to be stored in the other likelihood when we are using the 
    relative lensing parameters (mu_rel, delta_t, delta_n)

    ARGUMENTS:
    ---------
    - img_likelihood: the likelihood from which the strain should be
                      modified
    - parameters: the dictionary with all the parameters

    RETURNS:
    --------
    - cache: the waveform cache object containing the modified information
    - updated_parameters: the update parameters to pass to the waveform
    """

    cache = copy.deepcopy(img_likelihood.waveform_generator._cache)
    cache['waveform']['plus'] = (1./np.sqrt(parameters['relative_magnification']))*np.exp(-1j*np.pi*parameters['delta_n'])*cache['waveform']['plus']
    cache['waveform']['cross'] = (1./np.sqrt(parameters['relative_magnification']))*np.exp(-1j*np.pi*parameters['delta_n'])*cache['waveform']['cross']
    
    cache['parameters']['luminosity_distance'] *= np.sqrt(parameters['relative_magnification'])
    cache['parameters']['n_phase'] += parameters['delta_n']
    
    updated_parameters = copy.deepcopy(cache['parameters'])


    return cache, updated_parameters

def make_strain_from_effective_parameters(img_likelihood, parameters):
    """
    Function converting the strain stored in one likelihood to the 
    strain to be stored in the other likelihood when we are using
    the effective parameters (hence luminosity_distance_2, geocent_time_2,
    n_phase_2)

    ARGUMENTS:
    ---------
    - img_likelihood: the likelihood from which the strain should be
                      modified
    - parameters: the dictionary with all the parameters

    RETURNS:
    --------
    - cache: the waveform cache object containing the modified information
    - updated_parameters: the update parameters to pass to the waveform
    """

    cache = copy.deepcopy(img_likelihood.waveform_generator._cache)
    mu_rel = cache['parameters']['luminosity_distance']/parameters['luminosity_distance_img2']
    delta_n = parameters['n_phase_img2']-cache['parameters']['n_phase']
    cache['waveform']['plus'] = mu_rel*np.exp(-1j*np.pi*delta_n)*cache['waveform']['plus']
    cache['waveform']['cross'] = mu_rel*np.exp(-1j*np.pi*delta_n)*cache['waveform']['cross']
    
    cache['parameters']['luminosity_distance'] = parameters['luminosity_distance_img2']
    cache['parameters']['n_phase'] = parameters['n_phase_img2']
    
    updated_parameters = copy.deepcopy(cache['parameters'])

    return cache, updated_parameters
    
def geocent_time_from_delta_t(parameters):
    """
    Function getting the geocentric time for the second image
    when the arguments passed are geocentric_time & delta_t

    ARGUMENTS:
    ---------
    - parameters: the parameters from which to read the info

    RETURNS:
    --------
    - geocent_time_img2: the geocentric time for the second image
    """

    geocent_time_img2 = parameters['geocent_time'] + parameters['delta_t']

    return geocent_time_img2

def geocent_time_from_geocent_time_img2(parameters):
    """
    Function getting the geocentric time for the second image
    when the arguments passed are geocentric_time_img1 & geocentric_time_img2

    ARGUMENTS:
    ---------
    - parameters: the parameters from which to read the info

    RETURNS:
    --------
    - geocent_time_img2: the geocentric time for the second image
    """

    geocent_time_img2 = parameters['geocent_time_img2']

    return geocent_time_img2



class JointGravitationalWaveTransient(Likelihood):
    """
    Class to perform joint parameter estimation. 
    This is a modification of the initial golum class build
    to work with the golum_pipe framework by using the initial
    likelihoods
    """

    def __init__(self, likelihood_img1, likelihood_img2, effective_parameters = False):
        """
        Initialization of the class

        ARGUMENTS:
        ----------
        - likelihood_img1: the likelihood for the first image
        - likelihood_img2: the likelihood for the second image
        - effective_parameters: whether we are using the effective parameters
                                or the relative lensing parameters
        """
        super(JointGravitationalWaveTransient,self).__init__()

        self.likelihood_img1 = likelihood_img1
        self.likelihood_img2 = likelihood_img2
        self.effective_parameters = effective_parameters

        if effective_parameters:
            self.lensing_parameters = ['geocent_time_img2', 'luminosity_distance_img2', 'n_phase_img2']
            self.conversion_function = make_strain_from_effective_parameters
            self.geocent_time_conv = geocent_time_from_geocent_time_img2
        else:
            self.lensing_parameters = ['relative_magnification', 'delta_t', 'delta_n']
            self.conversion_function = make_strain_from_relative_lensing_parameters
            self.geocent_time_conv = geocent_time_from_delta_t

        self.noise_likeli = self.noise_log_likelihood()

        self.parameters = dict()

    def noise_log_likelihood(self):
        """
        Function setting the noise likelihood once and for all
        """

        return self.likelihood_img1.noise_log_likelihood()+self.likelihood_img2.noise_log_likelihood()
        
    def log_likelihood_ratio(self):
        """
        Function computing the log likelihood ratio for the two image
        """

        params_img1 = dict()
        for key in self.parameters.keys():
            if key not in self.lensing_parameters and "_img2" not in key:
                params_img1[key.replace('_img1', '')] = self.parameters[key]

        # pass the parameters to the likelihood
        self.likelihood_img1.parameters = params_img1.copy()
        log_l = self.likelihood_img1.log_likelihood_ratio()

        # add the parameters to cache
        img2_cache, img2_parameters = self.conversion_function(self.likelihood_img1, self.parameters)

        self.likelihood_img2.waveform_generator._cache = img2_cache
        for key in self.parameters.keys():
            if key not in img2_parameters.keys() and key not in self.lensing_parameters and "_img1" not in key:
                img2_parameters[key.replace('_img2', '')] = self.parameters[key]
        img2_parameters['geocent_time'] = self.geocent_time_conv(self.parameters)
        self.likelihood_img2.parameters = img2_parameters.copy()
        log_l += self.likelihood_img2.log_likelihood_ratio()

        return log_l
        
    def log_likelihood(self):
        """
        Function computing the log likelihood by using the functions
        defined above
        """

        return self.noise_likeli + self.log_likelihood_ratio()
