{%- assign subject_1 = analysis.subjects[0] -%}
{%- assign subject_2 = analysis.subjects[1] -%}
{%- assign ifos_subject_1 = subject_1.meta['interferometers'] -%}
{%- assign ifos_subject_2 = subject_2.meta['interferometers'] -%}

[output_settings]
outdir = {{ production.rundir }}
label = {{ production.name }}
plot-corner = True
overwrite-outdir = False
logging-level = INFO
result-format = json 

[analysis_settings]
duration = {{ subject_1.meta['data']['segment length'] }}
minimum-frequency = { {% for ifo in ifos_subject_1 %}{{ifo}}:{{subject_1.meta['quality']['minimum frequency'][ifo]}},{% endfor %}}
maximum-frequency = { {% for ifo in ifos_subject_1 %}{{ifo}}:{{subject_1.meta['quality']['maximum frequency'][ifo]}},{% endfor %}}
sampling-frequency = {{ subject_1.meta['quality']['sample-rate']  | round }}
reference-frequency = 20
waveform-generator= bilby.gw.waveform_generator.WaveformGenerator
catch-waveform-errors = True
waveform-approximant = {{  analysis['meta']['waveform']['approximant'] | default : IMRPhenomXPHM }}
frequency-domain-source-model = golum.tools.waveformmodels.lensed_bbh_model
trigger-time-image-1 = {{ subject_1.meta['event time'] }}
trigger-time-image-2 = {{ subject_2.meta['event time'] }}
detectors-image-1 = {{  ifos_subject_1 }}
detectors-image-2 = {{ ifos_subject_2  }}

[sampler_settings]
sampler = {{ analysis['meta']['sampler']['sampler'] | default: "dynesty" }}
sampling-seed = {{ analysis['meta']['sampler']['seed'] | default : None }}
n-parallel = {{ analysis['meta']['sampler']['parallel jobs'] | default:2}}
sampler-kwargs = {{ analysis['meta']['sampler']['sampler kwargs'] |  default: "{'nlive': 10, 'naccept' : 5, 'check_point_plot' : True, 'check_point_delta_t' : 1800, 'print_method' : 'interval-60', 'samples' : 'acceptance-walk'}"  }}

[prior_settings]
prior-file = jpe_prior.prior 
prior-dict = None

[golum_settings]
likelihood = golum.pe.likelihood.JointGravitationalWaveTransient 
compute-coherence-ratio = False
unlensed-file-image-1 = None
unlensed-file-image-2 = None
use-effective-parameters = False

[event_1_settings]
channel-dict = None
psd-dict = None
spline-calibration-envelope-dict = None
calibration-model = None
spline-calibration-nodes = 10
spline-calibration-amplitude-uncertainty-dict = None
spline-calibration-phase-uncertainty-dict = None
calibration-prior-boundary = reflective

[event_2_settings]
channel-dict = None
psd-dict = None
spline-calibration-envelope-dict = None
calibration-model = None
spline-calibration-nodes = 10
spline-calibration-amplitude-uncertainty-dict = None
spline-calibration-phase-uncertainty-dict = None
calibration-prior-boundary = reflective

[injection_settings_image_1]
injection = True
injection-file = None
injection-dict = {"mass_1" : "35.4", "mass_2" : 26.7, "a_1" : 0.4, "a_2" : 0.3, "tilt_1" : 0.5, "tilt_2" : 1.0, "phi_12" : 1.7, "phi_jl" : 0.3, "luminosity_distance" : 1000, "dec" : -0.6, "ra" : 0.9, "theta_jn" : 0.4, "psi" : 2.659, "phase" : 0.8, "geocent_time" : 1126259642.413, "n_phase" : 0.}
injection-from-lensing-parameters = False
lensing-parameters-dict = None
injection-numbers = [None]
injection-waveform-approximant = None
gaussian-noise = True
n-simulation = 1
enforce-signal-duration = False
generation-seed = 12

[injection_settings_image_2]
injection = True
injection-dict = {"mass_1" : "35.4", "mass_2" : 26.7, "a_1" : 0.4, "a_2" : 0.3, "tilt_1" : 0.5, "tilt_2" : 1.0, "phi_12" : 1.7, "phi_jl" : 0.3, "luminosity_distance" : 1500, "dec" : -0.6, "ra" : 0.9, "theta_jn" : 0.4, "psi" : 2.659, "phase" : 0.8, "geocent_time" : 1126269642.413, "n_phase" : 0.5}
injection-file = None
injection-from-lensing-parameters = False
lensing-parameters-dict = None
injection-numbers = [None]
injection-waveform-approximant = None
gaussian-noise = True
n-simulation = 1
enforce-signal-duration = False
generation-seed = 30

[condor_settings]
accounting={{ analysis['meta']['scheduler']['accounting group'] }}
accounting_user = {{ config['condor']['user'] }}
local=False
local-generation={{ analysis['meta']['scheduler']['local generation'] | default: False }}
local-plot=False
periodic-restart-time={{ analysis['meta']['scheduler']['periodic restart time'] | default: 28800 }}
request-memory={{ analysis['meta']['scheduler']['request memory'] | default: 8.0}}
request-disk={{ analysis['meta']['scheduler']['request disk'] | default: 5.0 }}
request-memory-generation=None
request-cpus={{ analysis['meta']['scheduler']['request cpus'] | default: 16}}
scheduler={{ analysis['meta']['scheduler']['type'] | default: "condor" }}
scheduler-args=None
scheduler-module=None
scheduler-env=None
transfer-files={% if analysis['meta']['scheduler']['osg'] %}True{% else %}{{ analysis['meta']['scheduler']['transfer files'] | default: False }}{% endif %}
log-directory=None
online-pe=False
osg={{ analysis['meta']['scheduler']['osg'] | default: False }}
desired-sites={{ analysis['meta']['scheduler']['desired sites'] | default: None }}
submit = False

[population settings]
selection-effects = False

[other_bilby_settings]
